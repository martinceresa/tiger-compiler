# Compiladores

Buenas!!

En este repo encontrarán las entregas de la materia de compiladores (lo más
actualizado posible), similar a lo que se encuentra en el [svn de la materia](http://dcc.fceia.unr.edu.ar:81/svn/lcc/T-521-Compiladores/Public/ "Compiladores").

La materia se dicta en Standard ML, y usamos el compilador [Moscow Ml](http://mosml.org/
"Moscow Ml").

Dentro de la carpeta **haskell** se encuentra mi intento de portear al menos las
diferentes entregas para motivar nuevos *tigres* en haskell.

El archivo *tiger.vim* es un (mal) 'resaltador' de sintaxis de Tiger para vim.


Haskell!!

Paquetes adicionales: cond, mtl
