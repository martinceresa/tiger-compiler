{-# Language TypeSynonymInstances #-}
{-# Language FlexibleInstances #-}
module TigerHInter where

import           TigerAbs

import           Data.Map as M
import qualified Data.Sequence as S

import           Control.Monad
import           Control.Monad.State


type Mem = Map Symbol TyRep

data TyRep = I Int | S Symbol | Arr (S.Seq TyRep) | Rec (Map Symbol TyRep) | NilRep | URep
    | FunTy [Symbol] [Exp]
    deriving Show
-- data St = St {output :: String, ret :: TyRep, mem :: Mem}

class (Monad w) => Evaluator w where
    varV :: Symbol -> w TyRep
    funB :: Symbol -> w TyRep
    printOut :: Symbol -> w ()
    --
    setVvar :: Symbol -> TyRep -> w ()
    --
    setVvarRec :: Symbol -> TyRep -> w ()
    destapar :: [Symbol] -> w ()

data Sta = Sta {
  venv :: Map Symbol [TyRep] ,
  output :: [Symbol]
               }

type Eva = State Sta

instance Evaluator Eva where
  varV s = do
    st <- get
    return $ head ((venv st) M.! s)
  funB = varV
  printOut s = do
    st <- get
    put $ st {output = s : ( output st )}
  setVvar s t = do
    st <- get
    let ms = (venv st) ! s
    let m' = case ms of
               [] -> insert s [t] (venv st)
               (_:xs) -> insert s (t : xs) (venv st)
    put $ st {venv = m'}
  setVvarRec s t = do
    st <- get
    let ms = (venv st) ! s
    let m' = insert s (t : ms) (venv st)
    put $ st {venv = m'}
  destapar xs = do
    st <- get
    let m' = Prelude.foldl (\ r x ->
                      let t = tail (r ! x) in insert x t r
                   ) (venv st) xs
    put $ st {venv = m'}

initSt :: Sta
initSt = Sta M.empty []


runEva e = evalState (pEvaluate e) initSt
    
getV :: (Evaluator w) => Var -> w TyRep
getV (SimpleVar s) = varV s
getV (FieldVar var s) = getV var >>= \t ->
        case t of
            Rec m -> return $ m ! s
            _ -> error "No es un record.FielVar"
getV (SubscriptVar v e) = do
    varray <- getV v
    eint <- evaluate' e
    case (varray, eint) of
        (Arr arr, I p) -> return $ S.index arr p
        _ -> error "Error de tipitos. SubscriptVar"

assVar :: (Evaluator w) => Var -> TyRep -> w ()
assVar (SimpleVar s) r = setVvar s r
assVar (SubscriptVar v e) r = do
        Arr se <- getV v
        (I p) <- pEvaluate e
        let se' = S.update p r se -- Es safe update!
        assVar v (Arr se')
assVar (FieldVar v s) r = do
        Rec m <- getV v
        let m' = insert s r m 
        assVar v (Rec m')

declarate :: (Evaluator w) => Dec -> w ()
declarate (FunctionDec xs) = mapM_ (\ (nm, args, _, bd, _) -> setVvar nm (FunTy (treateArgs args) (preEvaluate bd))) xs
declarate (VarDec vn _ _ init _) = pEvaluate init >>= setVvar vn
declarate (TypeDec _ ) = return () -- Ya paso esta etapa.

treateArgs :: [Field] -> [Symbol]
treateArgs = Prelude.map (\ (x, _ , _) -> x)

preEvaluate :: Exp -> [Exp]
preEvaluate x = [x] -- hacer

operate :: Oper -> TyRep -> TyRep -> TyRep
operate o (I x) (I y) = case o of
                        PlusOp  -> I (x + y)
                        MinusOp -> I (x - y)
                        TimesOp -> I (x * y)
                        DivideOp -> I (div x y)
                        EqOp -> I (if x == y then 1 else 0)
                        NeqOp -> I (if x == y then 0 else 1)
                        LtOp -> I (if x < y then 1 else 0)
                        LeOp -> I (if x <= y then 1 else 0)
                        GtOp -> I (if x > y then 1 else 0)
                        GeOp -> I (if x >= y then 1 else 0)

pEvaluate :: (Evaluator w) => Exp -> w TyRep
pEvaluate = evaluate . preEvaluate

evaluate :: (Evaluator w) => [Exp] ->  w TyRep
evaluate [] = return URep
evaluate [x] = evaluate' x
evaluate (x:xs) | BreakExp _ <- x = return URep
                | otherwise = evaluate' x >> evaluate xs
-- evaluate'
evaluate' :: (Evaluator w) => Exp -> w TyRep
evaluate' (VarExp v _) = getV v
evaluate' (BreakExp _) = error "No deberíamos estar acá"
evaluate' (UnitExp _) = return URep -- La pateamos para adelante...
evaluate' (NilExp _) = return NilRep
evaluate' (IntExp i _) = return $ I i
evaluate' (StringExp s _) = return $ S $ toSym s
evaluate' (CallExp f as _)  = do
            as' <- mapM pEvaluate as
            FunTy args bd <- funB f
            -- setBkPoint
            -- tapar args
            mapM_ (\ (x , y) -> setVvarRec x y) (zip args as')
            r <- evaluate bd
            destapar args
            -- restBkPoint
            return r
evaluate' (OpExp l o r _ ) = do
            l' <- pEvaluate l
            r' <- pEvaluate r
            return $ operate o l' r'
evaluate' (RecordExp flds s _) = do
            flds' <- mapM (\ (c, e) -> pEvaluate e >>=
                            \e' -> return (c,e')) flds  -- Ya se hizo el type check...
            return $ Rec (fromList flds')
evaluate' (SeqExp xs _ ) = evaluate xs
evaluate' (AssignExp v e _) = do
            e' <- pEvaluate e
            assVar v e'
            return URep
evaluate' (IfExp c th Nothing _) = do
            (I cond) <- pEvaluate c
            if cond == 0 then return URep
                        else pEvaluate th
evaluate' (IfExp c th (Just el) _) = do
            (I cond) <- pEvaluate c
            if cond == 0 then pEvaluate el
                        else pEvaluate th
evaluate' (WhileExp c bd p ) = do
            (I cond) <- pEvaluate c
            if (cond == 0)
                then return URep
                else evaluate [bd, WhileExp c bd p]
evaluate' (ForExp s _ lo hi bd p) = do
            (I loI) <- pEvaluate lo
            (I hiI) <- pEvaluate hi
            -- tapar [s]
            setVvarRec s (I loI)
            evaluate $ concat (Prelude.map (\i -> [bd , (AssignExp (SimpleVar s) (IntExp i p) p)]) [(loI + 1) .. (hiI + 1)])
            destapar [s]
            return URep
evaluate' (LetExp ds bd _) = do
            mapM_ declarate ds
            pEvaluate bd
evaluate' (ArrayExp s long init _) = do
            (I l) <- pEvaluate long
            as <- replicateM l (pEvaluate init)
            return $ Arr $ S.fromList as
