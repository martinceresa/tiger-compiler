{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE TypeFamilies         #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE UndecidableInstances #-}
module TigerTrans where

import qualified Control.Monad.State as ST
import           Prelude             hiding (EQ, GT, LT, error, exp, seq)
import qualified Prelude             as P (error)
import qualified TigerAbs            as Abs
import           TigerEnv
import           TigerErrores
import           TigerFrame          as F
import           TigerTemp
import           TigerTree

import           Control.Monad
import qualified Data.Foldable       as Fold
import           Data.List           as List
import           Data.Ord            hiding (EQ, GT, LT)

import qualified Data.Text           as T

import           Debug.Trace

type TransFrag = Frag -- Reexport Fragtype

data BExp = Ex Exp | Nx Stm | Cx ((Label, Label) -> Stm)

instance Show BExp where
    show (Ex e) = "Ex " ++ show e
    show (Nx e) = "Nx " ++ show e
    show (Cx _ ) = "Cx "

type Level = [(Frame, Int)]

getFrame :: Level -> Frame
getFrame ((f,_):_) = f

getNlvl :: Level -> Int
getNlvl ((_,i):_) = i

setFrame :: Frame -> Level -> Level
setFrame f ((_,l):xs) = (f,l) : xs

newLevel :: Level -> T.Text -> [Bool] -> Level
newLevel [] s bs = [(newFrame s bs,0)]
newLevel ls@((pF,lvl):_) s bs = (newFrame s bs, lvl+1) : ls

getParent :: Level -> Level
getParent [] = P.error "No fuimos del outermost level"
getParent (_:xs) = xs

outermost :: Level
outermost = [(newFrame (T.pack "_undermain") [],-1) ]

class (Monad w, TLGenerator w, Daemon w) => FlorV w where
    -- |Level managment
    getActualLevel :: w Int
    upLvl :: w ()
    downLvl :: w ()
    -- | Salida managment
    pushSalida :: Maybe Label -> w ()
    topSalida :: w (Maybe Label)
    popSalida :: w()
    -- |Level managment Cont.
    pushLevel :: Level -> w ()
    popLevel  :: w ()
    topLevel  :: w Level
    allocLocal :: Bool -> w Access
    allocLocal b = do
        t <- topLevel
        popLevel
        (f,acc) <- F.allocLocal (getFrame t) b
        let nt = setFrame f t
        pushLevel nt
        return  acc
    allocArg :: Bool -> w Access
    allocArg b = do
        t <- topLevel
        popLevel
        (f,a) <- F.allocArg (getFrame t) b
        pushLevel (setFrame f t)
        return a
    -- | Frag managment
    pushFrag  :: Frag -> w ()
    getFrags  :: w [Frag]


class IrGen w where
    procEntryExit :: Level -> BExp -> w ()
    unitExp :: w BExp
    nilExp :: w BExp
    intExp :: Int -> w BExp
    stringExp :: T.Text -> w BExp
    simpleVar :: Access -> Int -> w BExp
    varDec :: Access -> w BExp
    fieldVar :: BExp -> Int -> w BExp
    subscriptVar :: BExp -> BExp -> w BExp
    recordExp :: [(BExp,Int)]  -> w BExp
    callExp :: Label -> Bool -> Bool -> Level -> [BExp] -> w BExp
    letExp :: [BExp] -> BExp -> w BExp
    breakExp :: w BExp
    seqExp :: [BExp] -> w BExp
    preWhileforExp :: w ()
    posWhileforExp :: w ()
    whileExp :: BExp -> BExp -> w BExp
    forExp :: BExp -> BExp -> BExp -> BExp -> w BExp
    ifThenExp :: BExp -> BExp -> w BExp
    ifThenElseExp :: BExp -> BExp -> BExp -> w BExp
    ifThenElseExpUnit :: BExp -> BExp -> BExp -> w BExp
    assignExp :: BExp -> BExp -> w BExp
    preFunctionDec :: Level -> w ()
    posFunctionDec :: w ()
    functionDec :: BExp -> Level -> Bool -> w BExp
    binOpIntExp :: BExp -> Abs.Oper -> BExp -> w BExp
    binOpIntRelExp :: BExp -> Abs.Oper -> BExp -> w BExp
    binOpStrExp :: BExp -> Abs.Oper -> BExp -> w BExp
    arrayExp :: BExp -> BExp -> w BExp

seq :: [Stm] -> Stm
seq [] = ExpS $ Const 0
seq [s] = s
seq (x:xs) = Seq x (seq xs)

unEx :: (Monad w,TLGenerator w) => BExp -> w Exp
unEx (Ex e) = return e
unEx (Nx s) = return $ Eseq s (Const 0)
unEx (Cx cf) = do
    r <- newTemp
    t <- newLabel
    f <- newLabel
    return $ Eseq
        (seq [
            Move (Temp r) (Const 1),
            cf(t,f),
            Label f,
            Move (Temp r) (Const 0),
            Label t])
        (Temp r)


unNx :: (Monad w,TLGenerator w) => BExp -> w Stm
unNx (Ex e) = return $ ExpS e
unNx (Nx s) = return s
unNx (Cx cf) = do
        t <- newLabel
        f <- newLabel
        return $ seq [cf(t,f),Label t, Label f]

unCx :: (Monad w,TLGenerator w, Daemon w) => BExp -> w ((Label, Label) -> Stm)
unCx (Nx s) = error $ internal $ strToErr "UnCx(Nx...)"
unCx (Cx cf) = return cf
unCx (Ex (Const 0)) = return (\(_,f) -> Jump (Name f) f)
unCx (Ex (Const _)) = return (\(t,_) -> Jump (Name t) t)
unCx (Ex e) = return (uncurry (CJump NE e (Const 0)))

instance (FlorV w) => IrGen w where
    procEntryExit lvl bd =  do
        bd' <- unNx bd
        let res = Proc bd' (getFrame lvl)
        trace ("procEntry") $ pushFrag res
    stringExp t = do
        l <- newLabel
        let ln = T.append (T.pack ".long ")  (T.pack $ show $ T.length t)
        let str = T.append (T.append (T.pack ".string \"") t) (T.pack "\"")
        pushFrag $ AString l ln str
        return $ Ex $ Name l
    preFunctionDec lvl = do
        pushSalida Nothing  -- In case a break is called.
        upLvl
        pushLevel lvl
    posFunctionDec = popSalida >> downLvl
    -- functionDec :: BExp -> Level -> Bool -> w BExp
    functionDec bd lvl proc = do
        body <- if proc then unNx bd
                else do
                        e <- unEx bd
                        return $ Move (Temp rv) e
        procEntryExit lvl (Nx body)
        return $ Ex $ Const 0
    simpleVar acc level = do
        l <- getActualLevel
        let rellev = l - level
        return $ Ex (exp acc rellev)
    varDec acc = do { i <- getActualLevel; simpleVar acc i}
    unitExp = return $ Ex (Const 0)
    nilExp = return $ Ex (Const 0)
    intExp i = return $ Ex (Const i)
    fieldVar be i = do
        bem <- unEx be
        t <- newTemp
        return $ Ex $
            Eseq
                (seq[Move (Temp t) bem, ExpS $ externalCall "_checkNil" [Temp t]]) -- Podemos agregar msj en runtime
                (Mem (Binop Plus (Const (wSz * i)) (Temp t)))
    -- subscriptVar :: BExp -> BExp -> w BExp
    subscriptVar var ind = do
        evar <- unEx var
        eind <- unEx ind
        tvar <- newTemp
        tind <- newTemp
        return $ Ex $
            Eseq
                (seq    [Move (Temp tvar) evar
                        ,Move (Temp tind) eind
                        ,ExpS $ externalCall "_checkIndex" [Temp tvar, Temp tind]])
                (Mem $ Binop Plus (Temp tvar) (Binop Mul (Temp tind) (Const wSz)))
    -- recordExp :: [(BExp,Int)]  -> w BExp
    recordExp flds = do
        eflds <- mapM (\(e,i) -> do
                                t <- newTemp
                                e' <- unEx e
                                let es = Move (Temp t) e'
                                -- let es = Eseq (Move (Temp t) e') (Temp t)
                                return (es, (Temp t,i))
                            ) flds
        let (bes,tes) = unzip eflds -- vale la pena hacer todo este maneje para mantener el orden de los efectos?
        let ordtes = sortBy (comparing snd) tes
        let lordtes = length ordtes
        ret <- newTemp
        return $ Ex $ Eseq
            (seq (bes ++ [ExpS $ externalCall "_allocRecord" (Const lordtes : map fst ordtes), Move (Temp ret) (Temp rv)]))
            (Temp ret)
    -- callExp :: Label -> Bool -> Bool -> Level -> [BExp] -> w BExp
    callExp name external isproc lvl args = do
        let nivel = getNlvl lvl
        let getMem = Fold.foldl (\ res _ -> Mem $ Binop Minus res (Const fpPrevLev)) (Temp fp) -- Depende la arq
        actlvl <- getActualLevel
        let fpLev   | nivel == actlvl = Mem $ Binop Minus (Temp fp) (Const fpPrevLev)
                    | nivel < actlvl = getMem [1..(actlvl - nivel + 1)] -- Mejorar...
                    | otherwise = Temp fp
        (argst, argseff) <- foldM (\ (ts,es) h -> case h of
                            Ex (Const n) -> return (Const n:ts, es)
                            Ex (Name n) -> return (Name n:ts, es)
                            Ex (Temp n) -> return (Temp n:ts, es)
                            _ -> do
                                t <- newTemp
                                h' <- unEx h
                                let mht = Move (Temp t) h'
                                return (Temp t:ts, mht : es)
                        ) ([],[]) args
        let argstSL = if external then argst else fpLev : argst
        if isproc then
            return $ Nx $ seq (argseff ++ [ExpS $ Call (Name name) argstSL])
        else
            do
                ret <- newTemp
                return $ Ex $ Eseq (seq $ argseff ++ [ExpS $ Call (Name name) argstSL, Move (Temp ret) (Temp rv)]) (Temp ret)
    -- letExp :: [BExp] -> BExp -> w BExp
    letExp [] e = do -- Puede parecer al dope, pero no...
            e' <- unEx e
            return $ Ex e'
    letExp bs body = do
        bes <- mapM unNx bs
        be <- unEx body
        return $ Ex $ Eseq (seq bes) be
    -- breakExp :: w BExp
    breakExp = do
        l <- topSalida
        case l of
            Just ll -> return $ Nx $ Jump (Name ll) ll
            Nothing -> error $ internal $ T.pack "Break en cualca"
    -- seqExp :: [BExp] -> w BExp
    seqExp [] = return $ Nx $ ExpS $ Const 0
    seqExp bes = do
        let ret = last bes
        case ret of
            Nx e' -> do
                bes' <- mapM unNx bes
                return $ Nx $ seq bes'
            Ex e' -> do
                    let bfront = init bes
                    ess <- mapM unNx bfront
                    return $ Ex $ Eseq (seq ess) e'
            _ -> error $ internal $ T.pack "WAT!123"
    -- preWhileforExp :: w ()
    preWhileforExp = do
        l <- newLabel
        pushSalida (Just l)
    -- posWhileforExp :: w ()
    posWhileforExp = popSalida
    -- whileExp :: BExp -> BExp -> Level -> w BExp
    whileExp cond body = do
        test <- unCx cond
        cody <- unNx body
        init <- newLabel
        bd <- newLabel
        lastM <- topSalida
        case lastM of
            Just last ->
                return $ Nx $ seq
                    [Label init
                    , test (bd,last)
                    , Label bd
                    , cody
                    -- , Label last esto es un error
                    , Jump (Name init) init
                    , Label last]
            _ -> error $ internal $ T.pack "no label in salida"
    -- forExp :: BExp -> BExp -> BExp -> BExp -> w BExp
    forExp lo hi var body = do
        lcond <- newLabel
        lbody <- newLabel
        lt <-topSalida
        last <- case lt of
                    Just t -> return t
                    _ -> error $ internal $ T.pack "no label in salida"
        clo <- unEx lo
        chi <- unEx hi
        cvar <- unEx var
        cbody <- unNx body
        case chi of
            Const i -> return $ Nx $ seq
                        [Move cvar clo
                        , Jump (Name lcond) lcond
                        , Label lbody
                        , cbody
                        , Move cvar (Binop Plus cvar (Const 1))
                        , Label lcond
                        , CJump GT cvar (Const i) last lbody
                        , Label last]
            _ -> do
                t <- newTemp
                return $ Nx $ seq
                        [Move cvar clo
                        ,Move (Temp t) chi
                        , Jump (Name lcond) lcond
                        , Label lbody
                        , cbody
                        , Move cvar (Binop Plus cvar (Const 1))
                        , Label lcond
                        , CJump LE cvar (Temp t) lbody last
                        , Label last]

    -- ifThenExp :: BExp -> BExp -> w BExp
    ifThenExp cond bod = do
        true <- newLabel
        false <- newLabel
        test <- unCx cond
        cbod <- unNx bod
        return $ Nx $ seq
                [test(true, false)
                , Label true
                , cbod
                , Jump (Name false) false
                , Label false
                ]
    -- ifThenElseExp :: BExp -> BExp -> BExp -> w BExp
    ifThenElseExp cond bod els = do
        true <- newLabel
        false <- newLabel
        last <- newLabel
        res <- newTemp
        test <- unCx cond
        cbod <- unEx bod
        cels <- unEx els
        return $ Ex $ Eseq (seq
                [ test(true, false)
                , Label true
                , Move (Temp res) cbod
                , Jump (Name last) last
                , Label false
                , Move (Temp res) cels
                , Jump (Name last) last
                , Label last
                ]) (Temp res)
    -- ifThenElseExpUnit :: BExp -> BExp -> BExp -> w BExp
    ifThenElseExpUnit _ _ _ = return $ Ex $ Const 0
    --     true <- newLabel
    --     false <- newLabel
    --     last <- newLabel
    --     test <- unCx cond
    --     cbod <- unEx bod
    --     cels <- unEx els
    --     return $ Ex $ seq
    --             [ test(true, false)
    --             , Label true
    --             , cbod
    --             , Jump (Name last) last
    --             , Label false
    --             , cels
    --             , Jump (Name last) last
    --             , Label last
    --             ]
    -- assignExp :: BExp -> BExp -> w BExp
    assignExp cvar cinit = do
        cvara <- unEx cvar
        cin <- unEx cinit
        case cvara of
            Mem v' ->  do
                t <- newTemp
                return $ Nx $ seq [Move (Temp t) cin, Move cvara (Temp t)]
            _ -> return $ Nx $ Move cvara cin
    -- binOpIntExp :: BExp -> Abs.Oper -> BExp -> w BExp
    binOpIntExp le op re = do
        l <- unEx le
        r <- unEx re
        let subst oper = \(t,f) -> CJump oper l r t f
        let subconst oper i j=  \(t,f) -> if (oper i j) then Jump (Name t) t else Jump (Name f) f
        return $ case (l,r,op) of -- Simplificamos un poco las cosas...
            (Const li, Const ri,Abs.PlusOp) -> Ex $ Const (li+ri)
            (Const li, Const ri,Abs.MinusOp) -> Ex $ Const (li-ri)
            (Const li, Const ri,Abs.TimesOp) -> Ex $ Const (li*ri)
            (Const li, Const ri,Abs.DivideOp) -> if ri == 0 then Ex$ Binop Div l r else  Ex $ Const (div li ri)
            (Const li, Const ri,Abs.EqOp) -> Cx $ subconst (==) li ri
            (Const li, Const ri,Abs.NeqOp) -> Cx $ subconst (/=) li ri
            (Const li, Const ri,Abs.LtOp) -> Cx $ subconst (<) li ri
            (Const li, Const ri,Abs.LeOp) -> Cx $ subconst (<=) li ri
            (Const li, Const ri,Abs.GtOp) -> Cx $ subconst (>) li ri
            (Const li, Const ri,Abs.GeOp) -> Cx $ subconst (>=) li ri
            (_,_,Abs.PlusOp) -> Ex $ Binop Plus l r
            (_,_,Abs.MinusOp) -> Ex $ Binop Minus l r
            (_,_,Abs.TimesOp) -> Ex $ Binop Mul l r
            (_,_,Abs.DivideOp) -> Ex $ Binop Div l r
            (_,_,Abs.EqOp) -> Cx $ subst EQ
            (_,_,Abs.NeqOp) -> Cx $ subst NE
            (_,_,Abs.LtOp) -> Cx $ subst LT
            (_,_,Abs.LeOp) -> Cx $ subst LE
            (_,_,Abs.GtOp) -> Cx $ subst GT
            (_,_,Abs.GeOp) -> Cx $ subst GE
    -- binOpIntRelExp :: BExp -> Abs.Oper -> BExp -> w BExp
    binOpIntRelExp _ _ _  = return $ Ex $ Const 0 -- not needed
    -- binOpStrExp :: BExp -> Abs.Oper -> BExp -> w BExp
    binOpStrExp strl op strr = do
        sl <- unEx strl
        sr <- unEx strr
        t <- newTemp
        let subst oper = \(t,f) -> CJump oper (Eseq (seq [ExpS $ externalCall "_stringCompare" [sl,sr], Move (Temp t) (Temp rv)]) (Temp t)) (Const 0) t f
        case op of
            Abs.EqOp -> return $ Cx $ subst EQ
            Abs.NeqOp -> return $  Cx $ subst NE
            Abs.LtOp ->  return $ Cx $ subst LT
            Abs.LeOp -> return $  Cx $ subst LE
            Abs.GtOp -> return $  Cx $ subst GT
            Abs.GeOp ->  return $ Cx $ subst GE
            _ -> error $ internal $ T.pack "Operación no permitida con strings..."
    -- arrayExp :: BExp -> BExp -> w BExp
    arrayExp size init = do
        sz <- unEx size
        ini <- unEx init
        t <- newTemp
        return $ Ex $ Eseq (seq
                [ExpS $ externalCall "_allocArray" [sz,ini]
                , Move (Temp t) (Temp rv)
                ]) (Temp t)

