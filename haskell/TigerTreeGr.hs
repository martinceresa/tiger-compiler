import           Control.Monad.State
import           Data.GraphViz.Types.Graph
import           Data.Map
import qualified TigerTemp                 as Temp
import qualified TigerTree                 as T

data FreeStm a = Move a a
    | ExpS a | Jump a Temp.Label
    | CJump T.Relop a a Temp.Label Temp.Label
    | Seq a a
    | Label Temp.Label
    deriving Show

data FreeExp a = Const Int
    | Name Temp.Label
    | Temp Temp.Temp
    | Binop T.BOp a a
    | Mem a
    | Call a [a]
    | Eseq a a
    deriving Show

data Est = Est {  ids :: Int
        , stmR        :: Map Int (FreeStm Int)
        , expR        :: Map Int (FreeExp Int)}

makeNodeS :: T.Stm -> [Int] -> FreeStm Int
makeNodeS (T.Move {}) (l:r:_) = Move l r
makeNodeS (T.ExpS _) (e:_) = ExpS e
makeNodeS (T.Jump _ l) (e:_) = Jump e l
makeNodeS (T.Seq _ _) (h:t:_) = Seq h t
makeNodeS (T.Label l) _ = Label l

makeNodeE :: T.Exp -> [Int] -> FreeExp Int
makeNodeE (T.Const i) _ = Const i
makeNodeE (T.Name l) _ = Name l
makeNodeE (T.Temp t) _ = Temp t
makeNodeE (T.Binop b _ _) (l:r:_) = Binop b l r
makeNodeE (T.Mem _) (l:_) = Mem l
makeNodeE (T.Call _ _) (f:as) = Call f as
makeNodeE (T.Eseq _ _) (s:e:_)  = Eseq s e

assignIdExp :: (FreeExp Int) -> State Est Int
assignIdExp e = do
    st <- get
    let nid = ids st + 1
    let nm = insert nid e (expR st)
    put $ st{ ids = nid, expR = nm }
    return nid

assignIdStm :: FreeStm Int -> State Est Int
assignIdStm s = do
    st <- get
    let nid = ids st + 1
    let nm = insert nid s (stmR st)
    put $ st{ ids = nid, stmR = nm }
    return nid

createTreeStm :: T.Stm -> State Est Int
createTreeStm e@(T.Move l r) = do
    l' <- createTreeExp l
    r' <- createTreeExp r
    assignIdStm $ Move l' r'
createTreeStm (T.ExpS e) = do
    e' <- createTreeExp e
    assignIdStm $ ExpS e'
createTreeStm (T.Jump e l) = do
    e' <- createTreeExp e
    assignIdStm $ Jump e' l
createTreeStm (T.CJump r e1 e2 l1 l2) = do
    e1' <- createTreeExp e1
    e2' <- createTreeExp e2
    assignIdStm $ CJump r e1' e2' l1 l2
createTreeStm (T.Seq s e) = do
    s' <- createTreeStm s
    e' <- createTreeStm e
    assignIdStm $ Seq s' e'
createTreeStm (T.Label l) = assignIdStm $ Label l

createTreeExp :: T.Exp -> State Est Int
createTreeExp (T.Const i) = assignIdExp $ Const i
createTreeExp (T.Name l) = assignIdExp $ Name l
createTreeExp (T.Temp t) = assignIdExp $ Temp t
createTreeExp (T.Binop b e1 e2) = do
    e1' <- createTreeExp e1
    e2' <- createTreeExp e2
    assignIdExp $ Binop b e1' e2'
createTreeExp (T.Mem e) = do
    e' <- createTreeExp e
    assignIdExp $ Mem e'
createTreeExp (T.Call f as) = do
    f' <- createTreeExp f
    as' <- mapM createTreeExp as
    assignIdExp $ Call f' as'
createTreeExp (T.Eseq s e) = do
    s' <- createTreeStm s
    e' <- createTreeExp e
    assignIdExp $ Eseq s' e'
