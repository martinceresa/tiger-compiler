{-# LANGUAGE DeriveDataTypeable   #-}
{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE StandaloneDeriving   #-}
{-# LANGUAGE TypeSynonymInstances #-}

module CoreQuoter where

import           CoreLanguage
import qualified CoreParser                    as CoreP
import           Data.Generics
import           Data.Text                     hiding (map)
import qualified Language.Haskell.TH           as TH
import           Language.Haskell.TH.Quote
import           Text.ParserCombinators.Parsec

deriving instance Data CoreStr
deriving instance Typeable CoreStr
-- deriving instance Data CoreScDefnStr
deriving instance Typeable ProgramStr

execute :: [(String,[String], CoreStr)] -> CoreProgram
execute xs = map (\(nm,arg,bd) -> (toText nm, map toText arg, fmap toText bd)) xs

quoteExprExp :: String -> TH.ExpQ

expr  :: QuasiQuoter
expr  =  QuasiQuoter { quoteExp = quoteExprExp}

program :: QuasiQuoter
program = QuasiQuoter {quoteExp = quoteProgramExp }

parseExpr :: Monad m => (String, Int, Int) -> String -> m CoreStr
parseExpr (file, line, col) s =
    case runParser p () "" s of
      Left err  -> fail $ show err
      Right e   -> return e
  where
    p = do  pos <- getPosition
            setPosition $
              (flip setSourceName) file $
              (flip setSourceLine) line $
              (flip setSourceColumn) col $
              pos
            spaces
            e <- CoreP.expr
            eof
            return e

parseProgram :: Monad m => (String, Int, Int) -> String -> m ProgramStr
parseProgram (file, line, col) s =
    case runParser p () "" s of
      Left err  -> fail $ show err
      Right e   -> return e
  where
    p = do  pos <- getPosition
            setPosition $
              (flip setSourceName) file $
              (flip setSourceLine) line $
              (flip setSourceColumn) col $
              pos
            spaces
            e <- CoreP.programs
            eof
            return e

quoteExprExp s =  do  loc <- TH.location
                      let pos =  (TH.loc_filename loc,
                                 fst (TH.loc_start loc),
                                 snd (TH.loc_start loc))
                      expr <- parseExpr pos s
                      dataToExpQ (const Nothing) expr

quoteProgramExp s =  do     loc <- TH.location
                            let pos =  (TH.loc_filename loc,
                                 fst (TH.loc_start loc),
                                 snd (TH.loc_start loc))
                            program <- parseProgram pos s
                            dataToExpQ (const Nothing) program
