module CorePrint where

import           CoreComp
import           CoreLanguage
import           CoreStats
import           CoreUtils

-- import CoreGM

showState :: TiState -> IO ()
showState st  = do
     putStrLn $ showHeap $ heap st
     putStrLn $ showStack (stack st) (heap st)
     putStrLn $ showStats $ stats st
     putStrLn $ "Dump:" ++ show (dump st)
     putStrLn $ "FINAL RES:" ++ show (output st)

showDStack :: TiState -> IO ()
showDStack st = do
        putStrLn $ showStack (stack st) (heap st)
        putStrLn $ "Dump:" ++ show (dump st)
        putStrLn $ showHeap $ heap st

showStack :: TiStack -> TiHeap -> String
showStack (st,num) h = "Size:" ++ show num ++ "\n"  ++ foldr (\s res ->
    (case hLookup h s  of
        NAp a1 a2 -> '(' : show a1 ++ "@" ++ show a2 ++ ")"
        NSupercomb nm arg bd -> "(" ++ show nm ++ "," ++ show arg ++ ","
                                ++ show bd ++ ")"
        NNum n ->  "N:" ++ show n
        NInd adr -> "*"++ show adr ++"*"
        NPrim nm prim -> show nm
        NData t bs -> "Pack{" ++ show t ++ "," ++ show bs ++ "}"
        NMarked _ n -> "MARKED?!:" ++ show n
    ) ++ ":" ++ res
         ) "FF" st
