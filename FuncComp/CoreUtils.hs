module CoreUtils where

import           Control.Arrow
import qualified Data.Map      as M
import           Data.Maybe
import qualified Data.Set      as S
import           Data.Text     (Text, append, pack)


-- A.1
-- CoreHeap Class ??
{-
       hInitial :: a -- | an initialised empty heap
       hAlloc :: a -> a -> (a, Addr) -- | a new heap and an address
       hUpdate :: a -> Addr -> a -> a
       hFree :: a -> Addr -> a
       hLookup :: a -> Addr -> a
       hAddresses :: a -> [Addr]
       hSize :: a -> Int
       hNull :: Addr -- | guaranteed to differ from every address returned by others
       hIsNull :: Addr -> Bool -- | hIsNull hNull = True, else False
-}
mapAccuml :: (a -> b -> (a,c)) -> a -> [b] -> (a,[c])
mapAccuml f z = foldr (\x (a,cs) -> let (a',c) = f a x in (a',c:cs)) (z,[])
-- Concrete Implementation
type Heap a = (M.Map Addr a, Int, HeapStats)

data HeapStats = HStat
    { num_alloc :: Int
    , num_frees :: Int
    }
    deriving Show

initHeapStats = HStat 0 0
incAlloc h = h{num_alloc = num_alloc h + 1}
incNAlloc n h = h{num_alloc = num_alloc h + n}

incFrees h = h{num_frees = num_frees h + 1}
incNFrees n h = h{num_frees = num_frees h + n}

-- type Addr = Maybe Int
type Addr = Int

showaddr :: Addr -> [Char]
showaddr adr = '#' : show adr

showHeap :: Show a => Heap a -> String
showHeap h = showHHeap h ++ showHStats h

showHHeap :: Show a => Heap a -> String
showHHeap (h,n,_) = M.foldrWithKey (\k v res ->
        showaddr k ++ "->" ++ show v ++ "\n"
        ++ res
        ) (". Mem:"++ show n ++"\n") h

showHStats :: Heap a -> String
showHStats (_,_,st) = show st

--instance CoreHeap (Heap a) where
hInitial :: Heap a
hInitial = (M.empty,1,initHeapStats)

hEquiv :: (Eq a) => Heap a -> Heap a -> Bool
hEquiv (h,_,_) h' = M.foldlWithKey' (\res k n ->
                ((hLookup h' k) == n) && res) True h

hAlloc :: Heap a -> a -> (Heap a, Addr)
hAlloc (h,s,st) a = let
                s' = s+1
                nh = (M.insertWith (\ _ _ -> error "YOU FOUL!") s' a) h
                ns = incAlloc st
             in ((nh,s',ns), s')

hUpdate :: Heap a -> Addr -> a -> Heap a
hUpdate (h,m,s) adr v = ((M.insert adr v) h,m,s)

hMap :: (a -> b) -> Heap a -> Heap b
hMap f (h,n,v)= (M.map f h,n , v)

hFree :: Heap a -> Addr -> Heap a
hFree (h,n,sts) a = if (M.member a  h)
            then ((M.delete a) h,n,incFrees sts)
            else error "Freeing shit! 1"

hLookup :: Heap a -> Addr -> a
hLookup (h,_,_) addr = case M.lookup addr h of
                        Just a -> a
                        Nothing -> error $ "looking for something?!" ++ show addr

hAddresses :: Heap a -> [Addr]
hAddresses (h,_,_) = M.keys h

hSize :: Heap a -> Int
hSize (h,_,_) = M.size h

hNull :: Addr
hNull = -1

hIsNull :: Addr -> Bool
hIsNull i = i == -1

-- A.2
-- Association List
--
type ASSOC a b = M.Map a [b]

aPush :: (Ord a) => [(a,b)] -> ASSOC a b -> ASSOC a b
aPush l g = M.unionWith (++) (afromList l) g

afromList :: (Ord a) => [(a,b)] -> ASSOC a b
afromList xs = M.fromList $ map (\(a,b) -> (a,[b])) xs

afoldl :: (a -> k -> b -> a) -> a -> ASSOC k b -> a
afoldl f a asc = M.foldlWithKey (\ a k bs -> f a k (head bs)) a asc

aLookupWithErr :: (Ord a) => ASSOC a b -> a -> String -> b
aLookupWithErr g a str | Just b <- aMLookup g a = b
                       | otherwise = error str

aMLookup :: (Ord a) => ASSOC a b -> a -> Maybe b
aMLookup asc a = head <$> M.lookup a asc

aLookup :: (Ord a) => ASSOC a b -> a -> b -> b
aLookup h a def = head $ M.findWithDefault [def] a h

aAssoc :: (Ord a) => ASSOC a b -> a -> b -> ASSOC a b
aAssoc g a b = M.insertWith (++) a [b] g

aInDom :: (Ord a) => ASSOC a b -> a -> Bool
aInDom = flip M.member

aDomain :: ASSOC a b -> [a]
aDomain = M.keys

aRange :: ASSOC a b -> [b]
aRange = concat . M.elems

aEmpty :: ASSOC a b
aEmpty = M.empty

aMap :: (b -> c) -> ASSOC a b -> ASSOC a c
aMap f xs = M.map (map f) xs

aConcat :: (Ord a) =>  ASSOC a b -> ASSOC a b -> ASSOC a b
aConcat asc abs = M.unionWith (++) asc abs

type Set a = S.Set a

setEmpty :: Set a
setEmpty = S.empty

setIsEmpty :: Set a -> Bool
setIsEmpty = S.null

setSingleton :: a -> Set a
setSingleton = S.singleton

setFromList :: Ord a => [a] -> Set a
setFromList = S.fromList
setToList :: Ord a =>  Set a -> [a]
setToList = S.toList

setUnion :: Ord a => Set a -> Set a -> Set a
setUnion = S.union

setIntersection :: Ord a => Set a -> Set a -> Set a
setIntersection = S.intersection

setSubtraction :: Ord a => Set a -> Set a -> Set a
setSubtraction = S.difference

setElemOf :: Ord a => a -> Set a -> Bool
setElemOf = S.member

setUnionList :: Ord a => [Set a] -> Set a
setUnionList = S.unions

---- Name Supply
--
type NameSupply = Int

initialNameSupply :: Int
initialNameSupply = 0

getName :: Int -> Text -> (NameSupply, Text)
getName ns prefix = (ns+1, makeName prefix ns)

getNames :: Int -> [Text] -> (NameSupply, [Text])
getNames ns prefixes = (ns + length prefixes, zipWith makeName prefixes [ns..])

makeName :: Text -> NameSupply -> Text
makeName prefix ns = append prefix (pack $ "_" ++ show ns)
