-- module CoreMain (main) where

import           CoreEval
import           CoreLanguage
import           CoreParser
import           CorePrint

import qualified System.Environment as Env
import           Text.Parsec        (runParser)


main = do
    s:xs <- Env.getArgs
    sourceCode <- readFile s
    let rawCoreAst = runParser expr () s sourceCode
    print rawCoreAst
