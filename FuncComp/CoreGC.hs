module CoreGC where

import           Data.List
import           Debug.Trace

import           CoreComp
import           CoreUtils

bigHeap :: Int
bigHeap = 100

gc :: TiState -> TiState
gc tiS = let
            (h1,st) = markFromStack (heap tiS) (stack tiS)
            (h2,d') = markFromDump h1 (dump tiS)
            (h3,glbs) = markFromGlobals h2 (sc_defs tiS)
            newHeap = scanHeap h3
        in
              tiS{heap = newHeap, stack = st, dump = d', sc_defs = glbs}

findStackRoots :: TiStack -> [Addr]
findStackRoots = get_Addrs

findDumpRoots :: TiDump -> [Addr]
findDumpRoots = const []

findGlobalRoots :: TiGlobals -> [Addr]
findGlobalRoots = aRange

-- MarkMachine
type MarkSt = (Addr, Addr, TiHeap)

initMarkSt h a = (a, hNull, h)

evalMark :: MarkSt -> MarkSt
evalMark st | markFinal st = st
            | otherwise = evalMark $ stepMark st

markFinal :: MarkSt -> Bool
markFinal (f,b,h) = case (hLookup h f) of
                            NMarked Done _ -> hIsNull b
                            _ -> False

stepMark :: MarkSt -> MarkSt 
stepMark (f,b,h) = case hLookup h f of
                    NAp a1 a2 -> (a1, f, hUpdate h f (NMarked (Visits 1) (NAp b a2)))
                    NMarked Done n -> case hLookup h b of
                                            NMarked (Visits 1) (NAp b' a2) -> (a2,b, hUpdate h b (NMarked (Visits 2) (NAp f b')))
                                            NMarked (Visits 2) (NAp a1 b') -> (b,b',hUpdate h b (NMarked Done (NAp a1 f)))
                                            NMarked (Visits j) (NData i as) -> 
                                                if j == i then
                                                        let b' = last as
                                                            ins = init as
                                                        in (b, b', hUpdate h b (NMarked Done (NData i (ins ++ [f]))))
                                                else
                                                    let
                                                        (pre, b':a:bs) = (take (j-1) as, drop (j-1) as)
                                                        in (a, b, hUpdate h b (NMarked (Visits (j+1)) (NData i (pre ++ [f,b'] ++ bs))))
                                        
                    NInd a -> (a, b, h)
                    NData i (a:as) -> (a,f, hUpdate h f (NMarked (Visits 1) (NData i (b:as))))
                    n -> (f, b, hUpdate h f (NMarked Done n))

markFrom h a = let (a', _, h') = evalMark $ initMarkSt h a
                in (h', a')
{-
   markFrom :: TiHeap -> Addr -> (TiHeap, Addr)
   markFrom h a = case hLookup h a of
                       NMarked _ -> (h,a)
                       NInd x -> let
                           (h', n) = markFrom h x
                           in (h', n)
                       NAp b n ->
                           let
                               (h1,b') = markFrom h b
                               (h2,n') = markFrom h1 n
                               h3 = hUpdate h2 a $ NMarked $ NAp b' n'
                           in (h3, a)
                       NData i addrs ->
                           let
                               (h', as) = foldr (\a (nh,as) ->
                                   let (nh',a') = markFrom nh a in (nh',a':as)) (h,[]) addrs
                           in (hUpdate h' a (NMarked $ NData i as), a)
                       n -> (hUpdate h a (NMarked n), a)
-}
markFromStack :: TiHeap -> TiStack -> (TiHeap, TiStack)
markFromStack iH (addrs,i) = 
            let (h', addrs') = foldr (\a (h',as) ->
                                    let (nh,a') = markFrom h' a in (nh, a':as)) (iH,[]) addrs
            in (h', (addrs',i))

markFromDump :: TiHeap -> TiDump -> (TiHeap,TiDump)
markFromDump = (,)

markFromGlobals :: TiHeap -> TiGlobals -> (TiHeap, TiGlobals)
markFromGlobals h env = 
        afoldl (\(h',nenv) ad nd ->
            let (nh,nd') = markFrom h' nd in
                (nh, aAssoc nenv ad nd')) (h,env) env

scanHeap :: TiHeap -> TiHeap
scanHeap h =
    let addrs = hAddresses h
    in foldr (\a hr ->
                case hLookup hr a of
                        NMarked _ n -> hUpdate hr a n
                        _ -> hFree hr a) h addrs
