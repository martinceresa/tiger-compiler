type MultiState = (Int, Int, Int,Int) -- (n,m,d,t)

evalMult :: MultiState -> [MultiState]
evalMult st | multFinal st = [st]
            | otherwise = st : (evalMult $ stepMult st)

stepMult :: MultiState -> MultiState
stepMult (n,m,0,t) = (n,m-1,n,t)
stepMult (n,m,d,t) = (n,m,d-1,t+1)

multFinal :: MultiState -> Bool
multFinal (n,0,0,t) = True
multFinal _ = False
