{-# LANGUAGE QuasiQuotes #-}
import           CoreLanguage
import           CoreQuoter
import           CoreTimEval

--test1 :: [(String,[String], CoreStr)]
test1 = [program|main = S K K 4|]
--test1 = [("main",[], [expr| S K K 4 |])]


test2 = [program|id = S K K; id1 = id id; main = id1 4|]

test3 = [program|main = 1 +2|]

four = [program|main = four + four; four = 2*2|]

simpletest = [program|main = if (0 > 9) 2 3|]

facto = [program|main = cons (factorial 5) nil; factorial x = if (x == 0) 1 (x*(factorial (x-1)))|]

fibo = [program|main = cons (fib 5) nil; fib n = if (n<2) 1 (fib (n-1) + fib (n-2))|]

testlet = [program|main = cons (f 1 2 3) nil; f x y z = let p = x+y in p+x+y+z|]

fx = [program|main = f 4; f x = let y = f 3 in g x y; g x y = 2|]

ex412 = [program|main =  f 1 2 3; fs p x y z = p + x + y + z; f x y z = fs (x+y) x y z|]

fletrec = [program|main = f 1; f x = letrec p = if (x==0) 1 q; q = if (x==0) p 2 in p+q|]

sillyb = [program|main = f; f x = letrec a = b; b = x in a|]

between = [program| main = between 1 4; between x y = if (x>y) nil (cons x (between (x+1) y))|]

cafEva =[program| main = cons (pepe * pepe) nil; pepe = 2+4|]

execute :: [(String,[String], CoreStr)] -> CoreProgram
execute xs = map (\(nm,arg,bd) -> (toText nm, map toText arg, fmap toText bd)) xs

run = last . eval . compile . execute
run' = eval . compile

evar = EVar . toText
