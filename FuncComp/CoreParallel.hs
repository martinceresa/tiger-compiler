module CoreParallel where

import           CoreLanguage
import           CoreUtils

type PgmState =
        ( PgmGlobalState -- Current global state
        , [PgmLocalState]) -- Current states of processors

-- Global Parallel GMachine State

data PgmGlobalState = PgmState
        {gmoutput  ::  GmOutput -- OutputStream
        ,gmheap    :: GmHeap -- Heap of nodes
        ,gmglobals :: GmGlobals -- Global addresses in heap
        ,gmsparks  :: GmSparks -- Sparked task pool
        ,gmstats   :: GmStats -- Statistics
        }
    --deriving Show

instance Show PgmGlobalState where
    show (PgmState oput heap gbls sparks sts) =
        "Global State! \n" ++
        "Output : " ++ show oput ++"\n" ++
        "Heap : " ++ show heap ++ "\n" ++
        "Sparks: " ++ show sparks++ "\n" ++
        "Stats : " ++ show sts++ "\n"

type GmOutput = [(Int,[String])]
putConstr :: Int -> Int -> GmOutput -> GmOutput
putConstr t n xs = (n,["Pack" ++ show t]) : xs

putNumber :: String -> GmOutput -> GmOutput
putNumber num [] = [(0,['#':num])]
putNumber num r@((n,sat):out)   | n > 0 = (n-1, num : sat) : out
                                | otherwise = (0,['#':num]) : r

type GmHeap = Heap Node

type GmGlobals = ASSOC Name Addr

search_Gbl :: Name -> GmGlobals -> String -> Addr
search_Gbl nm gbl str = aLookupWithErr gbl nm str

type GmSparks = [PgmLocalState]

emptyTask :: PgmLocalState
emptyTask = PgmLState [] [] [] [] 0

isEmp :: PgmLocalState -> Bool
isEmp (PgmLState [] [] [] [] 0) = True
isEmp _ = False

type GmStats = [Int]

-- Machine Specs
--
machineSize :: Int -- Core #
machineSize = 4

idleProc :: PgmState -> Int
idleProc (_,xs) = machineSize - length xs
--
-- Local State
--
data PgmLocalState = PgmLState
    {gmcode   :: GmCode -- Instruction Stream
    ,gmstack  :: GmStack -- Pointer stack
    ,gmdump   :: GmDump -- Stack of dump items
    ,gmvstack :: GmVStack -- Value stack
    ,gmclock  :: GmClock} -- Number of ticks the task has been ctive
    deriving (Show,Eq)

data Instruction
    = Unwind
    | Pushglobal Name
    | Pushint Int
    | Push Int
    | Mkap
    | Update Int
    | Pop Int
    | Slide Int -- Mark3
    | Alloc Int
-- Mark4
    | Eval
    | Add | Sub | Mul | Div | Neg
    | Eq | Ne | Lt | Le | Gt | Ge
    | Cond GmCode GmCode
-- Mark 5
    | Pack Int Int
    | Casejump [(Int, GmCode)]
    | Split Int
    | Print
-- Mark 7
    | Pushbasic Int
    | Mkbool | Mkint
    | Get
    | Return --Ex 3.46
-- Parallel
    | Par
    deriving (Eq, Show,Read)

data Node
    = NNum Int -- Numbers
    | NAp Addr Addr -- Applications
    | NGlobal Int GmCode -- Globals
    | NInd Addr -- Indirections
    | NConstr Int [Addr] -- Constructor
    | NLAp Addr Addr PgmPendingList
    | NLGlobal Int GmCode PgmPendingList
    deriving (Show,Eq)

type PgmPendingList = [PgmLocalState]

type GmCode = [Instruction]

type GmStack = [Addr]

type GmDump = [GmDumpItem]

type GmDumpItem = (GmCode,GmStack,GmVStack)

type GmVStack = [Int]

emptyVStack :: GmVStack
emptyVStack = []

type GmClock = Int

----
-- SC compilation
----
type GmCompiledSC = (Name, Int, GmCode)

type GmCompiler = CoreExpr -> GmEnvironment -> GmCode
type GmEnvironment = ASSOC Name Int -- Name, offset

getArg :: Node -> Addr
getArg (NAp _ a) = a
getArg (NLAp _ a _) = a
getArg _ = error "getArg err"

---
type GmState = (PgmGlobalState, PgmLocalState) -- Single 'parallel' step

type GmStateTran = (GmState -> GmState)

putOutput :: GmOutput -> GmState -> GmState
putOutput o (g,l) = (g{gmoutput = o},l)

putHeap :: GmHeap -> GmState -> GmState
putHeap h (g,l) = (g{gmheap = h},l)

putSparks :: GmSparks -> GmState -> GmState
putSparks s (g,l) = (g{gmsparks = s},l)

putStats :: GmStats -> GmState -> GmState
putStats s (g,l) = (g{gmstats = s},l)

getOutput :: GmState -> GmOutput
getOutput = gmoutput . fst

getHeap :: GmState -> GmHeap
getHeap = gmheap . fst

getGlobals :: GmState -> GmGlobals
getGlobals = gmglobals . fst

putGlobals :: GmGlobals -> GmState -> GmState
putGlobals gbl (g,l) = (g{gmglobals = gbl},l)

getSparks :: GmState -> GmSparks
getSparks = gmsparks . fst

getStats :: GmState -> GmStats
getStats = gmstats . fst

putCode :: GmCode -> GmStateTran
putCode c (g,l) = (g,l{gmcode=c})

putStack :: GmStack -> GmStateTran
putStack s (g,l) = (g,l{gmstack=s})

putDump :: GmDump -> GmStateTran
putDump d (g,l) = (g,l{gmdump = d})

putVStack :: GmVStack -> GmStateTran
putVStack v (g,l) = (g,l{gmvstack = v})

putClock :: GmClock -> GmStateTran
putClock c (g,l) = (g,l{gmclock = c})

getCode :: GmState -> GmCode
getCode = gmcode . snd

getStack :: GmState -> GmStack
getStack = gmstack . snd

getDump :: GmState -> GmDump
getDump = gmdump . snd

getVStack :: GmState -> GmVStack
getVStack = gmvstack . snd

getClock :: GmState -> GmClock
getClock = gmclock . snd
