module CoreTimEval where

import           CoreLanguage
import           CorePrelude
import           CoreTim
import           CoreUtils

compile :: CoreProgram -> TimState
compile program = TimState
        [Enter (Label (toText "main") (lkup (toText "main") offsets))]
        FrameNull -- Fp
        FrameNull -- DFp
        -- initialArgStack
        [initClosure]
        initialValueStack
        initialDump
        -- hInitial
        hCAF
        fpG --compiled_code
        statInitial
        [] -- initial output
    where
    -- Print stuff
    (h', fp) = fAlloc hInitial [Nothing, Nothing]
    initClosure = Just (topCont, fp)
    -- End Print stuff
    ((hCAF,fpG),offsets) = allocateInitialHeap compiled_sc_defs
    sc_defs = preludeDefs ++ simpleExtraPrelude ++ program
    compiled_sc_defs = map (compileSC initial_env) sc_defs
    initial_env = afromList $ map (\(nm,off,b) ->
                            if b
                                then
                                    (nm, Label nm off)
                                else
                                    (nm, Code [PushMarker off, Enter $ Label nm off])) offsets
            --afromList [(name , Label name) | (name , args , body) <- sc_defs]
            --
lkup :: Name -> [(Name, Int, b)] -> Int
lkup n ((m,i,_):xs) | n == m = i
                    | otherwise = lkup n xs

allocateInitialHeap :: [(Name,[Instruction])] -> ((TimHeap, CodeStore),[(Name,Int,Bool)])
allocateInitialHeap compiled_code = ((heap, global_frame_addr), offsets)
    where
        indexed_code = zip [1..] compiled_code
        offsets = map (\(off,(nm,cd)) ->
                            (nm,off, case cd of
                                        (Take n a:_) | a > 0 -> False
                                        _ -> True)) indexed_code
        closures = map (\(off,(nm,code)) -> case code of
                                                (Take n a:_) | a > 0 -> Just (code, global_frame_addr)
                                                _ -> Just (PushMarker off : code, global_frame_addr)) indexed_code
        (heap, global_frame_addr) = fAlloc hInitial closures

type TimCompilerEnv = ASSOC Name TimAMode

compileSC :: TimCompilerEnv -> CoreScDefn -> (Name , [Instruction])
compileSC env (name , args , body ) =
    case args of
        [] -> (name , Take ds 0 : ins)
        _ -> (name , ins')
    where
        ins' = if ln == 0 then ins else UpdateMarkers ln : Take ds ln : ins
        ln = length args
        (ds,ins) = compileR body new_env ln
        new_env = aConcat (afromList (zip args (map mkUpdIndMode [1..]))) env

compileR :: CoreExpr -> TimCompilerEnv -> Int -> (Int, [Instruction])
compileR e@(ENum n) p d = compileB e p [Return] d
compileR e@(EAp (EAp (EVar op) _) _) p d | isOp op = compileB e p [Return] d
-- compileR e@(EAp (EAp (EAp (EVar ifop) _) _) _) p d | isIf ifop = compileB e p [Return] d
compileR (ELet b bds bd) p d =
    let n = length bds
        (dn,ams,xs,_) = foldr (\(x,e) (dp,ams,xs,i) ->
            let (dj,amj) = compileU e (d+i) pb dp in (dj, (Move (d+i) amj) : ams,x:xs,i+1)) (d+n, [],[],1) bds
        p' = aPush (zip xs (map (\x -> mkIndMode (d+x)) [1..]))  p
        pb = if b then p' else p
        (d',is) = compileR bd p' dn
    in (d', ams ++ is)
compileR (EAp e a@(EVar _)) p d = -- supercomb or local variable
    let (d1,is) = compileR e p d in (d1, Push (compileA a p) : is)
compileR (EAp e n@(ENum _)) p d = -- constant
    let (d1,is) = compileR e p d in (d1, Push (compileA n p) : is)
compileR (EAp ef ea) p d =
    let
        (d1,ama) = compileU ea (d+1) p (d+1)
        (d2,isf) = compileR ef p d1
    in
        (d2, (Move (d+1) ama)
            :(Push (mkIndMode (d+1)))
            : isf)
compileR e@(EVar v) p d = let am = compileA e p in (d,mkEnter am)
compileR (EConstr t a) p d  | a > 0= (d,[UpdateMarkers a, Take a a, ReturnConstr t])
                            | otherwise = (d,[ReturnConstr t])
compileR (ECase e alts) p d =
    let (dn,alts') = foldr (\x (da,xs) -> let (di,x') = compileE x p d in (max da di,x':xs)) (d,[]) alts
        (d',ise) = compileR e p dn
    in (d', Push (Code [Switch alts']) : ise)
compileR _ _ _ = error "compileR can't do this yet"

mkUpdIndMode :: Int -> TimAMode
mkUpdIndMode i = Code [PushMarker i, Enter $ Arg i]
mkIndMode :: Int -> TimAMode
mkIndMode n = Code [Enter $ Arg n]


compileA ::CoreExpr -> TimCompilerEnv -> TimAMode
compileA (EVar v) p = aLookupWithErr p v ("Unknown variable" ++ (show v))
compileA (ENum b) p = IntConst b

compileB :: CoreExpr -> TimCompilerEnv
        -> [Instruction] -- continuation
        -> Int
        -> (Int,[Instruction])
compileB (EAp (EAp (EVar op) e1) e2) p cont d | isOp op  =  --compileB e2 p (compileB e1 p (Op (opsTtoO op) : cont))
    let
        (d1,e1') = compileB e1 p (Op (opsTtoO op) : cont) d
        (d2,e2') = compileB e2 p e1' d
        in (max d1 d2, e2')
compileB (ENum n) p cont d = (d,PushV (IntVConst n) : cont)
compileB e p cont d =
    let (d', is) = (compileR e p d)in (d', Push (Code cont) : is)

compileU :: CoreExpr -> Int -> TimCompilerEnv -> Int -> (Int,TimAMode)
compileU e@(ENum n) u p d = (d,IntConst n)
compileU e u p d = let (d', is) = compileR e p d in (d', Code (PushMarker u : is))

compileE :: CoreAlt -> TimCompilerEnv -> Int -> (Int, [Instruction])
compileE (i, bds, bd) p d = (d', ismvs ++ isbd)
    where
        p' = aPush (zipWith (\a b -> (a,Arg b)) bds [d+1..]) p
        (d',isbd) = compileR bd p' (d+(length bds))
        ismvs = map (\i -> Move (d+i) (Data i)) [1..length bds]

eval :: TimState -> [TimState]
eval state = state : rest_states
    where
        rest_states | timFinal state = []
                    | otherwise = eval next_state
        next_state = doAdmin (step state)

incSteps :: TimState -> TimState
incSteps st | (Take t n : ins) <- instr st = applyToStats (statIncnSteps n) st
incSteps st = applyToStats statIncSteps st

adminStackSize :: TimState -> TimState
adminStackSize st = applyToStats (stackSizeC $ length $ stack st) st

doAdmin :: TimState -> TimState
doAdmin s = adminStackSize $
            incSteps s

timFinal st = null $ instr st

applyToStats :: (TimStats -> TimStats) -> TimState -> TimState
applyToStats f st = st{stats= f $ stats st}

step :: TimState -> TimState
step st | (Take t n : ins) <- instr st =
    let
        stk = stack st
        (h',fp') = fAlloc (heap st) (take n stk ++ (genEmpty (t-n))) in if length stk >= n
                                        then st{instr = ins, fp=fp', stack = drop n stk, heap = h'}
                                        else error "Too few args for Take instruction"
step st | [Enter am] <- instr st = let Just (ins, fp') = amToClosure am (fp st) (dfp st) (heap st) (store st) in
                    st{instr=ins, fp = fp'}
step st | (Push am : ins) <- instr st = st{instr = ins, stack = amToClosure am (fp st) (dfp st) (heap st) (store st) : (stack st)}
step st | [Return] <- instr st =
    case (stack st) of
            [] -> let
                    (fu,x,s) : d = dump st
                    (n:v) =  if null (vstack st) then error ("No puede ser"++ show (stack st)) else vstack st
                    h'= fUpdate (heap st) fu x (Just (intCode,FrameInt n))
                    in st{stack = s, dump = d, heap = h'}
            (Just (i,f) : s) -> st{instr = i, fp = f, stack = s} -- (4.11)
step st | ((PushV m):i) <- instr st = case m of
                            Fp ->  st{instr = i, vstack = (getFpInt $ fp st) : vstack st} -- (4.12)
                            (IntVConst n) -> st{instr=i, vstack = n : (vstack st)} -- (4.14)
step st | ((Op Neg):i) <- instr st = let (n:v) = vstack st in st{instr=i, vstack = (negate n):v}
step st | ((Op op):i) <- instr st =
    let (n1:n2:v) = vstack st
        st'  = st{instr=i} in  case op of
                Add -> st'{vstack = (n1+n2):v}
                Sub -> st'{vstack = (n1-n2):v}
                Mult -> st'{vstack = (n1*n2):v}
                Div -> st'{vstack = (div n1 n2):v}
                Gr -> st'{vstack = (codeB $ n1 > n2):v}
                GrEq -> st'{vstack = (codeB $ n1 >= n2):v}
                Lt -> st'{vstack = (codeB  $ n1 < n2):v}
                LtEq -> st'{vstack = (codeB $ n1 <= n2):v}
                Eq -> st'{vstack = (codeB $ n1 == n2):v}
                NotEq -> st'{vstack = (codeB $ n1 /= n2):v}
{-
   step st | [Cond t f] <- instr st = -- (4.13)
       let (b:v) = vstack st in if (b /= 0) then st{instr=f,vstack=v}
                                           else st{instr=t,vstack=v}
-}
step st | (Move i a : is) <- instr st =
    let fpa = fp st
        ho = heap st
        ca = amToClosure a fpa (dfp st) ho (store st)
        h' = fUpdate ho fpa i ca
    in st{instr=is, heap = h'}
step st | (PushMarker x : is ) <- instr st = st{instr = is
                                            , stack = []
                                            , dump = (fp st, x, stack st) : (dump st)}
step st | (UpdateMarkers n : i) <- instr st =
    if (length $ stack st) >= n then st{instr=i}
    else
        let
            (fu,x,s):d = dump st
            (h',f') = fAlloc (heap st) (stack st)
            i' = fst $ foldr (\x (is,i) -> (Push (Arg i) : is,i+1)) (UpdateMarkers n:i,1) (stack st)
            h'' = fUpdate (heap st) fu x (Just (i',f'))
        in st{stack = stack st ++ s, dump = d, heap = h''}
step st | [Switch alts] <- instr st =
        let (t:v) = vstack st
            i = if (length alts < t)
                then error ("AAA:" ++ show (length alts) ++ "--" ++  show t)
                else alts !! (t - 1)
        in st{instr = i, vstack = v}
step st | [ReturnConstr t] <- instr st =
        case stack st of
            [] ->
                let
                    (fu,x,s):d = dump st
                    h' = fUpdate (heap st) fu x (Just ([ReturnConstr t], (fp st)))
                in st{stack= s, dump = d, heap = h'}
            (Just (i,f')):s -> st{instr=i
                                , stack = s
                                , vstack = t : (vstack st)
                                , fp = f'
                                , dfp = fp st}
step st | (Print : is) <- instr st =
        let (n:s) = vstack st
        in st{instr = is, vstack = s, output = (output st)++[n]}

codeB :: Bool -> Int
codeB True = 2
codeB False = 1

amToClosure :: TimAMode -> FramePtr -> FramePtr -> TimHeap -> CodeStore -> Closure
amToClosure (Arg n) fptr _ heap _ = fGet heap fptr n
amToClosure (Data n) _ fptr heap _ = fGet heap fptr n
amToClosure (Code il) fptr _ heap _ = mkClosure (il,fptr)
amToClosure (Label l i) _ _ heap fpG =
        fGet heap fpG i -- mkClosure (codeLookup cstore l, fptr)
amToClosure (IntConst n) fptr _ _ _ = mkClosure (intCode, FrameInt n)

mkEnter :: TimAMode -> [Instruction]
mkEnter (Code i) = i
mkEnter o  = [Enter o]
