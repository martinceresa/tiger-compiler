module CoreStats where

data TiStats = Stats
        {ticks      :: Int
        , superreds :: Int
        , primreds  :: Int
        , maxstack  :: Int
        }
    deriving Show

showStats :: TiStats -> String
showStats (Stats ticks sred pred mstack) =
    "Ticks:" ++ show ticks ++
    "\nSupercomb Reductions:" ++ show sred ++
    "\nPrim Reductions:" ++ show pred ++
    "\nMaxDepth Stack:" ++ show mstack

-- Exercise 2.7

tiStatInitial :: TiStats
tiStatInitial = Stats 0 0 0 0

tiStatMaxDepth :: Int -> TiStats -> TiStats
tiStatMaxDepth i s = s{maxstack = max (maxstack s) i}
tiStatIncSteps :: TiStats -> TiStats
tiStatIncSteps s = s{ticks=ticks s + 1}

tiStatIncSuperReds :: TiStats -> TiStats
tiStatIncSuperReds s = s{superreds = superreds s + 1}

tiStatIncPrimReds :: TiStats -> TiStats
tiStatIncPrimReds s = s{primreds = primreds s + 1}

tiStatGetSteps :: TiStats -> Int
tiStatGetSteps = ticks
