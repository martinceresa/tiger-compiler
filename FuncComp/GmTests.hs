{-# LANGUAGE QuasiQuotes #-}
import           CoreGM
import           CoreGMEval
import           CoreLanguage
import           CoreQuoter

--test1 :: (String,[String], CoreStr)
--test1 = ("main",[], [expr| S K K K 3 |])
test1 = [program|main = S K K 3|]

test2 = [("main",[], [expr| twice twice id 3 |])
        ,("id",["x"],[expr|x|])
        ,("twice",["f","x"],[expr|f (f x)|])]

test_letrecposta =
    [(toText "pair", [toText "x",toText "y",toText "f"]
        , EAp (EAp (EVar $ toText "f") (EVar $ toText "x")) (EVar $ toText "y"))
    ,(toText "fst", [toText "p"]
        , EAp (EVar $ toText "p") (EVar $ toText "K"))
    ,(toText "snd", [toText "p"]
        , EAp (EVar $ toText "p") (EVar $ toText "K1"))
    ,(toText "f", [toText "x", toText "y"]
        , ELet recursive [(toText "a", EAp (EAp (EVar $ toText "pair") (EVar $ toText "x")) (EVar $ toText "b")),
                          (toText "b", EAp (EAp (EVar $ toText "pair") (EVar $ toText "y")) (EVar $ toText "a"))]
                 (EAp (EVar $ toText "fst") (EAp (EVar $ toText "snd") (EAp (EVar $ toText "snd") (EAp (EVar $ toText "snd") (EVar $ toText "a"))))))
    ,(toText "main", [], EAp (EAp (EVar $ toText "f") (ENum 3)) (ENum 4))
    ]

yop = [(toText "Y",[toText "f"], ELet recursive
            [(toText "x", EAp (evar "f") (evar "x"))] (evar "x"))
        ,(toText "main",[], evar "Y")]

arithmetic = [("main",[],[expr|4 * 5 + (2-5)|])]

mark5intro = [("main",[], [expr| 3 + 4 * 5 |])]

simplest = [("main",[], [expr| 3|])]

simpleif = [("main",[], [expr| if (3>2) 1 2 |])]

facto = [("fact", ["n"], [expr| if (n==0) 1 (n* (fact (n-1)))|]),
        ("main", [], [expr|fact 7|])]

execute :: [(String,[String], CoreStr)] -> CoreProgram
execute xs = map (\(nm,arg,bd) -> (toText nm, map toText arg, fmap toText bd)) xs

run = last . eval . compile . execute
run' = eval . compile

evar = EVar . toText
