module CorePP where

import           CoreLanguage
import           Text.PrettyPrint

import           Data.Text        (Text, unpack)

ppprog :: CoreProgram -> String
ppprog = render . ppPrograms

ppPrograms :: CoreProgram -> Doc
ppPrograms xs = sep $ punctuate semi $ map ppProgram xs

ppProgram :: ScDefn Text -> Doc
ppProgram (nm, bds, bd) = text (unpack nm)
                        <+> (sep $ map (text . unpack) bds)
                        <+> equals
                        <+> nest 4 (ppExpr bd)

ppExpr :: Expr Text -> Doc
ppExpr (EVar n) = text $ unpack n
ppExpr (ENum i) = int i
ppExpr (EConstr t a) = text "Pack{" <> int t <> comma <> int a <> text "}"
ppExpr (EAp f x@(EAp _ _)) = (ppExpr f) <+> parens (ppExpr x)
ppExpr (EAp f x) = (ppExpr f) <+> (ppExpr x)
ppExpr (ELet rec bds bd)    | rec  = text "letrec" <+> (cat $ punctuate semi (map ppBds bds)) <+> text "in" <+> ppExpr bd
                            | otherwise = text "let" <+>(cat $ punctuate semi (map ppBds bds)) <+> text "in" <+> ppExpr bd
ppExpr (ECase c alts) = text "case" <+> parens (ppExpr c) <+> text "of"
                        $+$ vcat (map ppAlt alts)
ppExpr (ELam as bd) = text "\\" <> (sep $ map (text . unpack) as) <> text "." <+> ppExpr bd

ppBds :: (Text ,Expr Text) -> Doc
ppBds (x,xbd) = text (unpack x) <+> equals <+> ppExpr xbd

ppAlt :: (Int, [Text], Expr Text) -> Doc
ppAlt (c, bds, bd) = int c <+> sep (map (text . unpack) bds) <+> text "->" <+> ppExpr bd
