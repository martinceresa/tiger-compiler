{-# LANGUAGE QuasiQuotes #-}
module CorePrelude where

import           CoreLanguage
import           CoreQuoter

preludeDefs :: CoreProgram
preludeDefs = [
     (toText "I", [toText "x"], EVar $ toText "x")
    ,(toText "K", [toText "x",toText "y"], EVar $ toText "x")
    ,(toText "K1", [toText "x",toText "y"], EVar $ toText "y")
    ,(toText "S", [toText "f",toText "g",toText "x"],
        EAp (EAp (EVar $ toText "f") (EVar $ toText "x"))
            (EAp (EVar $ toText "g") (EVar $ toText "x")))
    ,(toText "compose", map toText ["f","g","x"],EAp (EVar $ toText "f")
            (EAp (EVar $ toText "g") (EVar $ toText "x")))
    ,(toText "twice", [toText "f"], EAp (EAp (EVar $ toText "compose") (EVar $ toText "f")) (EVar $ toText "f"))
    ]

execute :: [(String,[String], CoreStr)] -> CoreProgram
execute xs = map (\(nm,arg,bd) -> (toText nm, map toText arg, fmap toText bd)) xs

simpleExtraPrelude :: CoreProgram
simpleExtraPrelude =
    [(toText "cons",[], EConstr 2 2)
    ,(toText "nil",[], EConstr 1 0)
    ,(toText "true",[], EConstr 2 0)
    ,(toText "false",[], EConstr 1 0)
    ,(toText "if",[toText "c",toText "t",toText "f"],
            ECase (EVar (toText "c")) [(1,[], EVar (toText "f")),(2,[], EVar (toText "t"))])
    ]



evar = EVar . toText

coreStrToExpr :: CoreStr -> CoreExpr
coreStrToExpr = fmap toText

extraPreludeDefs :: CoreProgram
extraPreludeDefs =
    [
    (toText "false",[toText "x", toText "y"], evar "y")
    -- Primitives as functions
    ,(toText "if", [], evar "I")
    ,(toText "true",[toText "x", toText "y"], evar "x")
    ,(toText "and",[toText "b1", toText "b2", toText "t", toText "f"]
            , coreStrToExpr [expr| b1 (b2 t f) f |])
    ,(toText "or",[toText "b1", toText "b2", toText "t", toText "f"]
                 ,coreStrToExpr [expr| b1 t (b2 t f)|])
    ,(toText "not",[toText "b1", toText "t", toText "f"],
            coreStrToExpr [expr|b1 f t|])
    ,(toText "xor",[toText "x",toText "y"],
           EAp (EAp (evar "or") (EAp (EAp (evar "and") (EAp (evar "not") (evar "x"))) (evar "y")))
                                (EAp (EAp (evar "and") (EAp (evar "not") (evar "y"))) (evar "x")))
    ,(toText "pair" , [toText "a", toText "b", toText "f"]
                    , coreStrToExpr [expr| f a b |])
    ,(toText "fst", [toText "p"]
                    , coreStrToExpr [expr|p K|])
    ,(toText "snd", [toText "p"]
                    , coreStrToExpr [expr|p K1|])
    ,(toText "casePair", [], evar "I")
    ,(toText "cons",[toText "x", toText "xs"
                    ,toText "cn", toText "cc"]
                    ,coreStrToExpr [expr|cc x xs|])
    ,(toText "nil",[toText "cn", toText "cc"], evar "cn")
    ,(toText "caseList",[], evar "I")
    ,(toText "head",[toText "xs"], fmap toText [expr| caseList xs abort K|])
    ,(toText "tail",[toText "xs"], fmap toText [expr| caseList xs abort K1|])
    ,(toText "printList",[toText "xs"], fmap toText [expr| caseList xs stop printCons|])
    ,(toText "printCons",[toText "h",toText "t"], fmap toText [expr| print h (printList t)|])
    ]
