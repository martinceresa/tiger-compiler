module CoreComp where
import           CoreLanguage
import           CoreStats
import           CoreUtils

import           Control.Arrow


-- Spine Stack
type TiStack = ([Addr], Int)

get_Addrs :: TiStack -> [Addr]
get_Addrs = fst

initialStack :: TiStack
initialStack = ([],0)

is_SEmpty :: TiStack -> Bool
is_SEmpty ([],n) | n == 0 = True
                 | otherwise = error "Internal Error. WAT?!"
is_SEmpty _ = False

lView :: TiStack -> [Addr]
lView = fst

lnSt :: TiStack -> Int
lnSt (_,i) = i

spine :: TiStack -> Addr
spine = Prelude.head . fst

tailSp :: TiStack -> [Addr]
tailSp = Prelude.tail . fst

dropSp :: Int -> TiStack  -> TiStack
dropSp i (ls,s) = (Prelude.drop i ls, s -i)

addSp  :: Addr -> TiStack -> TiStack
addSp a (ls,s) = (a:ls, s+1)

-- Dump
type TiDump = [Int]

initialTiDump :: TiDump
initialTiDump = [0]

dump_push :: TiStack -> TiDump -> TiDump
dump_push st d = (lnSt st) : d

dump_top :: TiDump -> Int
-- dump_top [] = 0
dump_top xs = Prelude.head xs

dump_pop :: TiDump -> TiDump
dump_pop = Prelude.tail

dump_isempty :: TiDump -> Bool
dump_isempty [x] = x == 0
dump_isempty _ = False


dump_stack :: TiStack -> TiDump -> Int
dump_stack s d = lnSt s - dump_top d

-- Heap
type TiHeap = Heap Node

data Node = NAp Addr Addr -- Applications
            | NSupercomb Name [Name] CoreExpr -- Supercombinators
            | NNum Int -- Literal numbers
            | NInd Addr  -- Indirections
            | NPrim Name Primitive -- Primitive
            | NData Int [Addr] -- Tag, list of components
            | NMarked MarkState Node -- marked node

data MarkState = Done | Visits Int
    deriving (Show,Eq)

instance Eq Node where
    (NAp a b) == (NAp a' b') = a == a' && b == b'
    (NSupercomb nm ns bd) == (NSupercomb nm' ns' bd') = nm == nm' && ns == ns' && bd == bd'
    (NNum i) == (NNum j) = i == j
    (NInd a) == (NInd b) = a == b
    (NPrim nm _ ) == (NPrim nm' _) = nm == nm'
    (NData i as) == (NData j as') = i == j && as == as'
    (NMarked st n) == (NMarked st' n') = n == n' && st == st'
    _ == _ = False

node_Addrs :: Node -> [Addr]
node_Addrs (NAp a1 a2) = [a1,a2]
node_Addrs (NInd x) = [x]
node_Addrs (NData _ x) = x
node_Addrs _ = []

instance Show Node where
    show (NAp a1 a2) = "NAp(" ++ show a1 ++ "," ++ show a2 ++ ")"
    show (NSupercomb nm abs e) = "NSupercomb" ++ show nm ++ ":" ++ show abs ++ show e
    show (NNum i) = "N:" ++ show i
    show (NInd ad) = "Ind:" ++ show ad
    show (NPrim nm _) = "Prim:" ++ show nm
    show (NData i adrs) = "Pack{" ++ show i ++ "," ++ show adrs ++ "}"
    show (NMarked st n) = "Marked:"++ show st ++"(" ++ show n ++ ")"

type Primitive = TiState -> TiState

-- Globals
type TiGlobals = ASSOC Name Addr -- Here we changed the semantic...

-- Stats
applyToStats :: (TiStats -> TiStats) -> TiState -> TiState
applyToStats f st = st{stats = f $ stats st}

data TiState = TSt  { stack   :: TiStack
                    , dump    :: TiDump
                    , heap    :: TiHeap
                    , sc_defs :: TiGlobals
                    , stats   :: TiStats
                    , output  :: [Int]}
    deriving Show

instance Eq TiState where
    tl == tr = hEquiv (heap tl) (heap tr)
                    && (stack tl) == (stack tr)
                    && (dump tl) == (dump tr)
                    && (sc_defs tl) == (sc_defs tr)
                    && (output tl) == (output tr)

-- wrapMain ::
-- Cons main_res []
-- NAp (NAp (NPrim (Cons)) main_addr) Nil
-- a1 = NAp cons_addr main_addr
-- a2 = NAp a1 Nil
wrapMain :: TiHeap -> TiGlobals -> Addr -> (TiHeap, Addr)
wrapMain h assoc a = let
    cons_addr = aLookupWithErr assoc (toText "cons") "Internal err 666(1)"
    nil_addr  = aLookupWithErr assoc (toText "nil") "Internal err 666(2)"
    printL_addr = aLookupWithErr assoc (toText "printList") "Internal err 666(3)"
    (h',a1) = hAlloc h (NAp cons_addr a)
    (h'',a2) = hAlloc h' (NAp a1 nil_addr)
    in hAlloc h'' (NAp printL_addr a2)


allocatePrim :: (TiHeap, TiGlobals) -> Name -> Primitive -> (TiHeap, TiGlobals)
allocatePrim (h,env) nm pr =
    let (h',addr) = hAlloc h (NPrim nm pr) in (h',aAssoc env nm addr)

allocateSc :: (TiHeap, TiGlobals) -> CoreScDefn -> (TiHeap, TiGlobals)
allocateSc (h,g) (nm, args, bd)=
    let
        (h',addr) = hAlloc h (NSupercomb nm args bd)
        glbs = aAssoc g nm addr
    in (h',glbs)

