module CoreEval where

import           Data.List

import           Debug.Trace

import CorePrint
import           CoreComp
import           CoreGC
import           CoreLanguage
import           CorePrelude
import           CoreStats
import           CoreUtils

data StepMade = RedSuper | SApp | DontCount

compile :: CoreProgram -> TiState
compile program = TSt initial_stack initialTiDump initial_heap globals tiStatInitial []
    where
        defs = program ++ preludeDefs ++ extraPreludeDefs
        (initial_heap', globals) = buildInitialHeap defs
        (initial_heap,pmain_addr) = wrapMain initial_heap' globals main_addr
        initial_stack = addSp pmain_addr initialStack
        main_addr = case aMLookup globals (toText "main") of
                        Nothing -> error "main is not defined"
                        Just addr -> addr

buildInitialHeap :: [CoreScDefn] -> (TiHeap, TiGlobals)
buildInitialHeap s =
            afoldl allocatePrim
                    (foldl allocateSc (hInitial,aEmpty) s) primitives

primitives :: ASSOC Name Primitive
primitives = afromList $    [(toText "negate", primNeg)
                            ,(toText "+",flip primArith (+))
                            ,(toText "-",flip primArith (-))
                            ,(toText "*",flip primArith (*))
                            ,(toText "/",flip primArith div)
                            -- ,(toText "If",primIf)
                            ,(toText ">",flip primComp (>))
                            ,(toText ">=",flip primComp (>=))
                            ,(toText "<",flip primComp (<=))
                            ,(toText "<=",flip primComp (<=))
                            ,(toText "==",flip primComp (==))
                            ,(toText "=!",flip primComp (/=))
                            -- ,(toText "CasePair",primPair)
                            -- ,(toText "CaseList",primList)
                            ,(toText "abort",const (error "Abort!!"))
                            ,(toText "print",primPrint)
                            ,(toText "stop",primStop)
                                ]

eval :: TiState -> [TiState]
eval st = st : rest_states
    where
        rest_states | tiFinal st = []
                    | otherwise = eval next_st
        next_st = doAdmin $ step st

incon :: TiState -> Bool
incon ti = let h = heap ti in
   foldl' (\t a -> 
            case hLookup h a of
                NAp a1 a2 -> a1 == a2 && t
                NMarked _ _ -> False
                _ -> t) True (hAddresses h) 

doAdmin :: TiState -> TiState
doAdmin st =   
    let
        gced = if hSize (heap st) > bigHeap then gc st else st
    in applyToStats tiStatIncSteps $
                applyToStats (tiStatMaxDepth  (lnSt $ stack st)) $
                gced

tiFinal :: TiState -> Bool
tiFinal st = is_SEmpty $ stack st

isDataNode :: Node -> Bool
isDataNode (NNum _) = True
isDataNode (NData _ _) = True
isDataNode _ = False

step :: TiState -> TiState
step st = dispatch $ hLookup (heap st) (spine $ stack st)
    where
        dispatch (NNum n) =  numStep st n
        dispatch (NData id addrs) = dataStep st id addrs
        dispatch (NAp a1 a2) = apStep st a1 a2
        dispatch (NSupercomb sc args bd) = scStep st sc args bd
        dispatch (NInd a) = indStep st a
        dispatch (NPrim nm prim) = primStep st prim

dataStep :: TiState -> Int -> [Addr] -> TiState
dataStep st _ _ =
                if (lnSt $ stack st) == 1 + dump_top (dump st) then
                         st{stack = dropSp 1 (stack st)
                            , dump = dump_pop (dump st)}
                else error $ "Do you want to aplicate simple data? Type Error?"

primStep :: TiState -> Primitive -> TiState
primStep st prim = prim st

primStop :: TiState -> TiState
primStop st | [0] <- dump st = st{stack = initialStack}
            | otherwise = error "Stop with non-empty dump"

cont :: TiState -> Addr -> Int -> TiState
cont st addr arg =
    let stack' = dropSp arg $ stack st
        ndump = dump_push stack' $ dump st
        stack'' = addSp addr stack'
    in st{stack=stack'', dump = ndump}

primPrint :: TiState -> TiState
primPrint st =
    let
        [b1,b2] = getArgs st
        b1_node = hLookup (heap st) b1
    in if isDataNode b1_node
        then case b1_node of
                NNum n -> if not $ dump_isempty (dump st)
                            then error $ "Print with empty dump" ++ show (dump st)
                            else
                            st{stack = addSp b2 initialStack -- $ stack st -- Acá está la cosa
                            ,output = n : (output st)}
                _ -> error "Not a number?!"
        else
            cont st b1 2

primList :: TiState -> TiState
primList st =
    let
        arg = getArgs st
        [list_addr, cn_addr, cc_addr] = arg
        list_node = hLookup (heap st) list_addr
        ar = spine $ stack'
        stack' = dropSp 3 $ stack st
    in if isDataNode list_node
        then case list_node of
            NData 1 [] -> -- Nil
                st{heap = hUpdate (heap st) ar (NInd cn_addr)
                    ,stack = stack'}
            NData 2 [x_addr,xs_addr] -> -- Cons x xs
                let
                    (h',midAddr) = hAlloc (heap st) (NAp cc_addr x_addr)
                    h'' = hUpdate h' ar (NAp midAddr xs_addr)
                in
                st{stack = stack', heap = h''}
            _ -> error "Incorrect data node?"
        else
            cont st list_addr 1

primPair :: TiState -> TiState
primPair st =
    let
        [par_addr, fun_addr] = getArgs st
        cn = hLookup (heap st) par_addr
        stk' = dropSp 2 $ stack st
        ar = spine stk'
    in if isDataNode cn
        then case cn of
                NData 1 [fst,snd] ->
                    let
                        (h', haddr) = hAlloc (heap st) (NAp fun_addr fst)
                        h'' = hUpdate h' ar (NAp haddr snd)
                    in st{stack =stk'
                                        ,heap = h''}
                NData _ _ -> error $ "Type error, Pair expected. Instead: " ++ show cn
        else
            cont st par_addr 2

primIf  :: TiState -> TiState
primIf st =
    let [c,t,e] = getArgs st
        cn = hLookup (heap st) c
        stk = dropSp 3 $ stack st
        ar = spine stk
    in if isDataNode cn
        then
            case cn of
                NData 1 [] -> st{stack = stk
                                ,heap = hUpdate (heap st) ar (NInd e)}
                NData 2 [] -> st{stack = stk
                                ,heap = hUpdate (heap st) ar (NInd t)}
                _ -> error "Expected bool?"
        else
            cont st c 3

primConstr :: TiState -> Int -> Int -> TiState
primConstr st t addrs =
    let
        bs = getArgs st
        stack' = dropSp (length bs) (stack st)
        an = spine stack'
    in
        if length bs /= addrs then
            error $ "more args are needed. bs:" ++ show bs
                    ++ show (length bs) ++ " | addrs" ++ show addrs
        else
         st{stack = stack'
        , heap = hUpdate (heap st) an (NData t bs)}

primNeg :: TiState -> TiState
primNeg st =
    let
        [b] = getArgs st
        bn = hLookup (heap st) b
        stk = dropSp 1 $ stack st
        a1 = spine stk
    in
        if isDataNode bn
        then let    (NNum n) = bn
                    heap' = hUpdate (heap st) a1 (NNum (negate n))
             in st{stack= stk, heap=heap'}
        else
            cont st b 1

primDyadic :: TiState -> (Node -> Node -> Node) -> TiState
primDyadic st op =
    let
        b:c:xs = getArgs st
        (bn,cn) = (hLookup (heap st) b, hLookup (heap st) c)
        stk' = dropSp 2 $ stack st
    in  if isDataNode bn
        then
            if isDataNode cn
            then let
                    ar = spine stk' in
                    st{stack = stk'
                        ,heap = hUpdate (heap st) ar (op bn cn)}
            else
                cont st c 2
        else
            cont st b 2

primComp :: TiState -> (Int -> Int -> Bool) -> TiState
primComp st op = primDyadic st (\l r ->
            case (l,r) of
                (NNum n1, NNum n2) -> fromBool (op n1 n2)
                _ -> error "Type error?")
        where
            fromBool False = NInd $ aLookupWithErr (sc_defs st) (toText "false") "No false?!"
            fromBool True = NInd $ aLookupWithErr (sc_defs st) (toText "true") "No true?!"

primArith :: TiState -> (Int -> Int -> Int)-> TiState
primArith st op = primDyadic st (\bn cn ->
                    case (bn,cn) of
                        (NNum n1, NNum n2) -> NNum (op n1 n2)
                        _ -> error "Type error primArith")

indStep :: TiState -> Addr -> TiState
indStep st a = st{stack = new_stack}
    where
        new_stack = addSp a (dropSp 1 (stack st))

numStep :: TiState -> Int -> TiState -- rule (2.7)
numStep st _ = if (lnSt $ stack st) == 1 + dump_top (dump st) then
                 st{stack = dropSp 1 $ stack st
                    , dump = dump_pop (dump st)}
                else error $ "Numbers are not functions (1). Stacksize = "
                    ++ show (lnSt $ stack st) ++ "vs " ++ show (dump st)

apStep :: TiState -> Addr -> Addr -> TiState
apStep st a1 a2 = case hLookup (heap st) a2 of
                    NInd a3 -> st{heap = hUpdate (heap st) (spine $ stack st) (NAp a1 a3)}
                    _ -> applyToStats tiStatIncPrimReds $ st{stack = addSp a1 $ stack st}

getArgs :: TiState -> [Addr]
-- TiHeap -> TiStack -> [Addr]
getArgs st = let s = stack st
                 s' = dropSp 1 s
                 h = heap st
                 d = dump st
             in
                 map (\n -> case hLookup h n of
                            NAp _ ar -> ar
                            _ -> error "No ap?") (take (dump_stack s' d) (get_Addrs s'))

scStep :: TiState -> Name -> [Name] -> CoreExpr -> TiState
scStep st nm args bd = 
             applyToStats tiStatIncSuperReds $ st{stack = stack', heap = new_heap}
    where
        heapL = heap st
        stack' = dropSp (length args) (stack st)
        an = spine stack'
        new_env = aPush arg_bindings (sc_defs st)
        new_heap = instantiateWithUpdate bd an heapL new_env
        arg_bindings =
            if (length args >= lnSt (stack st)) -- Exercise 2.6
            then error $ show nm ++ " Applied to few arguments :" ++ show (length args) ++ ">=" ++ show (lnSt (stack st)) ++ "|| Args:" ++ show args ++ "\n Stack" ++ show (stack st)
            else zip args (getArgs st)

instantiate :: CoreExpr -> TiHeap -> TiGlobals -> (TiHeap, Addr)
instantiate (ENum n) heap _ = hAlloc heap (NNum n)
instantiate r@(EAp f x) heap glbs = let
                (hf, faddr) = instantiate f heap glbs
                (hx, xaddr) = instantiate x hf glbs
                res = (NAp faddr xaddr)
            in hAlloc hx res
instantiate (EVar v) heap glbs =
           (heap, aLookupWithErr glbs v ("Undefinied name:" ++ (show v)))
instantiate (ELet False bindings body) heap glbs  =
            let (instBds,nms,hp) = Data.List.foldl (\(inst,nms,h) (nm,bd) ->
                    let (h',addr) = instantiate bd h glbs in
                        ( addr : inst,nm : nms, h')) ([],[],heap) bindings
                new_glbs = aPush (zip nms instBds) glbs
            in instantiate body hp new_glbs
instantiate (ELet True bindings body ) heap glbs =
            let (instBds,nms,hp) = Data.List.foldl (\(inst,nms,h) (nm,bd) ->
                    let (h',addr) = instantiate bd h new_glbs in
                        ( addr : inst,nm : nms, h')) ([],[],heap) bindings
                new_glbs = aPush (zip nms instBds) glbs
            in instantiate body hp new_glbs
instantiate (EConstr t a) h s =  hAlloc h (NPrim (toText $ "Pack{" ++ show t ++ "," ++ show a ++ "}")
                                                 (\st -> primConstr st t a))
instantiate (ECase _ _) _ _ =  error "Can't instantiate case exprs"

instantiateWithUpdate :: CoreExpr -- Body of Supercomb
                        -> Addr -- Address of node to update
                        -> TiHeap -- Heap before istantiation
                        -> ASSOC Name Addr --  Associate parameters to addresses
                        -> TiHeap -- Heap after instantiation
instantiateWithUpdate (ENum n) upd_addr h _ = hUpdate h upd_addr (NNum n)
instantiateWithUpdate m@(EAp e1 e2) upd_addr heap env =
    let
        (h1, faddr) = instantiate e1 heap env
        (h2, xaddr) = instantiate e2 h1 env
        newnode =NAp faddr xaddr
    in
        hUpdate h2 upd_addr newnode
instantiateWithUpdate (EVar v) upd_addr h env =
    hUpdate h upd_addr (NInd vddr)
    where vddr = aLookupWithErr env v ("Undefinied name:" ++ (show v))
instantiateWithUpdate (ELet False bds bd) upd_addr heap glbs =
    let (instBds,nms,hp) = Data.List.foldl (\(inst,nms,h) (nm,bd) ->
                    let (h',addr) = instantiate bd h new_glbs in
                        ( addr : inst,nm : nms, h')) ([],[],heap) bds
        new_glbs = aPush (zip nms instBds) glbs
    in instantiateWithUpdate bd upd_addr hp new_glbs
instantiateWithUpdate (ELet True bindings body ) upd_addr heap glbs =
            let (instBds,nms,hp) = Data.List.foldl (\(inst,nms,h) (nm,bd) ->
                    let (h',addr) = instantiate bd h new_glbs in
                        ( addr : inst,nm : nms, h')) ([],[],heap) bindings
                new_glbs = aPush (zip nms instBds) glbs
            in instantiateWithUpdate body upd_addr hp new_glbs
instantiateWithUpdate (EConstr t a) upd_addr h env =
        hUpdate h upd_addr (NPrim (toText $ "Pack{" ++ show t ++ "," ++ show a ++ "}") (\st -> primConstr st t a))
instantiateWithUpdate _ _ _ _ = error "TODO"

-- Ex 2.15
-- a : a1 : a2 : []           d h [a:NPrim Add, a1 : NAp a b, a2 : NAp a1 c, b :
-- NNum n, c : NNum m]
-- > a2 : []                  (a1:[]):(a2:[]):d h [a2 : NNum (n + m)]
--
-- a : a1 : a2 : []           d h [a:NPrim Add, a1 : NAp a b, a2 : NAp a1 c]
-- >        c : []    ((b:[]):(a:a1:a2:[])):d
-- Estoy evaluando primero c acá, pero podría ser de la otra forma...
--
-- (2.8) ya la tenemo
