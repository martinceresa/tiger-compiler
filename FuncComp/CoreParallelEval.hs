{-# LANGUAGE QuasiQuotes #-}
module CoreParallelEval where

import           CoreLanguage
import           CoreParallel
import           CorePrelude
import           CoreQuoter
import           CoreUtils

import           Debug.Trace

eval :: PgmState -> [PgmState]
eval st = st : restStates
    where
        restStates  | gmFinal st = []
                    | otherwise = eval $ doAdmin $ steps st

steps :: PgmState -> PgmState
steps st = scheduler glb lcl
    where
        gst = fst st
        idlP = idleProc st
        sparks = gmsparks gst
        (newtasks,rest) = if idlP > 0
                   then (take idlP sparks, drop idlP sparks)
                   else ([],sparks)
        glb = PgmState (gmoutput gst) (gmheap gst) (gmglobals gst) rest (gmstats gst)
        lcl = (snd st) ++ newtasks

scheduler :: PgmGlobalState -> [PgmLocalState] -> PgmState
scheduler global tasks =
    (global', nonRunning ++ tasks')
    where
        (run,nonRunning) = numbersDropTake machineSize  tasks
        running = map tick run
        (global',tasks') = mapAccuml step global running

numbersDropTake :: Int -> [PgmLocalState] -> ([PgmLocalState],[PgmLocalState])
numbersDropTake 0 xs = ([],xs)
numbersDropTake n [] = ([],[])
numbersDropTake n (x:xs)  | isInWHNF x = numbersDropTake n xs
                          | otherwise = let (r,nr) = numbersDropTake (n-1) xs
                                        in (x:r,nr)

isInWHNF :: PgmLocalState -> Bool
isInWHNF = null . gmcode

makeTask :: Addr -> PgmLocalState
makeTask addr = PgmLState [Eval] [addr] [] [] 0

tick :: PgmLocalState -> PgmLocalState
tick s = s{gmclock=1+gmclock s}

gmFinal :: PgmState -> Bool
gmFinal s@(e,[]) = null $ gmsparks e
gmFinal _ = False

step :: PgmGlobalState -> PgmLocalState -> GmState
step gbl lcl = dispatch i (putCode is state)
    where
        (i:is) = getCode state
        state = (gbl,lcl)


doAdmin :: PgmState -> PgmState
doAdmin (stg,local) = (stg{gmstats = sts},lcl)
    where
        (lcl,sts) = foldr (\s (xs,stat) -> if isEmp s then (xs,stat)
                                            else
                                             case gmcode s of
                                            [] -> (xs,gmclock s : stat)
                                            _ -> (s:xs,stat)) ([],gmstats stg) local

type Transition = GmState -> GmState

dispatch :: Instruction -> GmState -> GmState
dispatch (Pushglobal f) = pushglobal f
dispatch (Pushint n)    = pushint n
dispatch Mkap = mkap
dispatch (Push n) = push n
dispatch Unwind = unwind
dispatch (Pop n) = pop n
dispatch (Update n) = update n
dispatch (Slide n) = slide n
dispatch (Alloc n) = alloc n
dispatch Eval = evalI
dispatch Add = arithmetic2 (+)
dispatch Sub = arithmetic2 (-)
dispatch Mul = arithmetic2 (*)
dispatch Div = arithmetic2 div
dispatch Neg = arithmetic1 negate
dispatch Eq  = comparison (btoI (==))
dispatch Ne = comparison (btoI (/=))
dispatch Lt = comparison (btoI (<))
dispatch Le = comparison (btoI (<=))
dispatch Gt = comparison (btoI (>))
dispatch Ge = comparison (btoI (>=))
dispatch (Cond c1 c2) = cond c1 c2
dispatch (Pack i1 i2) = pack i1 i2
dispatch (Casejump xs) = casejump xs
dispatch (Split i) = gsplit i
dispatch Print = gprint
dispatch (Pushbasic n) = pushbasic n
dispatch Mkbool = mkbool
dispatch Mkint = mkint
dispatch Get = gget
dispatch Return = retu
dispatch Par = par

par :: GmState -> GmState
par st = putSparks (makeTask a:t) $ putStack s st
    where
        (a:s) = getStack st
        t = getSparks st

retu :: Transition
retu st =
        putDump d $
        putCode i $
        putStack ((last stk) : s) $
        putVStack v st
    where
        stk = getStack st
        (i,s,v) : d = getDump st

btoI :: (Int -> Int -> Bool) -> Int -> Int -> Int
btoI f p q = if f p q then 2 else 1

gget :: Transition
gget st = case hLookup (getHeap st) a of
                NConstr t [] -> putStack d $ putVStack (t:v) st -- (3.44)
                NNum t -> putStack d $ putVStack (t:v) st -- (3.45)
                NInd e -> putStack (e:d) $ putCode (Get : (getCode st)) st
                e -> error $ "No value to put into the VSTack" ++ show e
    where
        a:d = getStack st
        v = getVStack st


mkprim :: (Int -> Node) -> Transition
mkprim f st = putVStack v $ putHeap h $ putStack (a:(getStack st)) st
    where
        t:v = getVStack st
        (h,a)  = hAlloc (getHeap st) (f t)

mkint :: Transition -- (3.43)
mkint = mkprim (\n -> NNum n)

mkbool :: Transition -- (3.42)
mkbool = mkprim (\n -> NConstr n [])

pushbasic :: Int -> Transition -- (3.41)
pushbasic n st = putVStack (n : (getVStack st)) st

pack :: Int -> Int -> Transition -- (3.30)
pack t n st = putHeap h $ putStack (a:s) st
    where
        as = getStack st
        (an,s) = (take n as, drop n as)
        (h,a) = hAlloc (getHeap st) (NConstr t an)

casejump :: [(Int,GmCode)] -> Transition -- (3.31)
casejump xs st = if t' /= t then
                    error "WAAATTTTTTT 123"
                    else putCode (i' ++ (getCode st)) st
    where
        (t', i') = xs !! (t - 1) -- Posible fuente de errores
        (a:s) = getStack st
        NConstr t _ = hLookup (getHeap st) a

gsplit :: Int -> Transition -- (3.32)
gsplit i st = putStack (xs ++ s) st
    where
        (a:s) = getStack st
        NConstr _ xs = hLookup (getHeap st) a

gprint :: Transition
gprint st =
    let
        (a:s) = getStack st
    in case hLookup (getHeap st) a of
            (NNum i) -> -- (3.33)
                putOutput (putNumber (show i) (getOutput st)) $ putStack s st
            (NConstr t xs) -> --(3.34)
                putStack (xs ++ s) $
                putOutput (putConstr t (length xs) (getOutput st)) $
                putCode (
                    (foldl (\xs _ -> Eval : Print : xs) [] [1..(length xs)])
                    ++ getCode st
                    ) st
            _ -> error "Not NFData??"

cond :: GmCode -> GmCode -> Transition
cond c1 c2 st = putCode (i ++ getCode st) $ putVStack v st
    where
        -- (a:s) = getStack st
        n:v = getVStack st
        i = if n == 2
            then c1 -- (3.46)
            else c2 -- (3.47)
                    -- NConstr 1 [] -> c2 -- (3.27)
                    -- NConstr 2 [] -> c1 -- (3.28)
                    -- _ -> error "Not boolean?"


boxInteger :: Int -> GmState -> GmState
boxInteger n st = putStack (a : getStack st) $ putHeap h' st
    where
        (h',a) = hAlloc (getHeap st) (NNum n)

boxBoolean :: Bool -> GmState -> GmState
boxBoolean b st =
        putStack (a: getStack st) $ putHeap h' st
    where
        (h',a) = hAlloc (getHeap st) (NConstr b' [])
        b'  | b = 2
            | otherwise = 1

comparison :: (Int -> Int -> Int) -> Transition
comparison = primitive2

unboxInteger :: Addr -> GmState -> Int
unboxInteger a st = case hLookup (getHeap st) a of
                        NNum n -> n
                        _ -> error "Unboxing a non-integer"

primitive1 ::
            (Int -> Int)
            ->  (GmState -> GmState) --state transition
primitive1 op st = -- (3.40)
    putVStack (op n : v) st
    where
        n : v = getVStack st
    -- box (op $ unbox a st) $ putStack s st
    -- where
    --     (a:s) = getStack st

primitive2 ::
            (Int -> Int -> Int)
            ->  Transition
primitive2 bop st = -- (3.39)
        putVStack (bop n0 n1 : v) st
    where
        n0:n1:v = getVStack st
    -- box (bop (unbox a1 st) (unbox a2 st)) $ putStack s st
    -- where
    --     (a1:a2:s) = getStack st

arithmetic1 :: (Int -> Int) -> Transition
arithmetic1 = primitive1

arithmetic2 :: (Int -> Int -> Int) -> Transition
arithmetic2 = primitive2

evalI :: Transition -- (3.23)
evalI st =
        putVStack emptyVStack $
        putCode [Unwind] $
        putStack [a] $
        putDump d' st
    where
        (a:s) = getStack st
        d = getDump st
        i = getCode st
        v = getVStack st
        d' = (i,s,v) : d

alloc :: Int -> Transition -- (3.20)
alloc n st = putHeap heap $ putStack (addrs ++ (getStack st)) st
    where (heap, addrs) =  foldr (\ x (h,as) -> let
                            (h',a) = hAlloc h (NInd hNull) in (h',a:as))
                             (getHeap st, []) [1..n]

update :: Int -> Transition -- (3.15)
update n st =
    let
        (a:as) = getStack st
        ust = unlock (as !! n) st
        h' = hUpdate (getHeap ust) (as !! n) (NInd a)
    in putStack as $ putHeap h' ust

pop :: Int -> Transition -- (3.16)
pop n st = putStack (drop n $ getStack st) st

parseInstr :: String -> Maybe (Int,Int)
parseInstr l@('P':'a':'c':'k':xs)= case read l of
                                Pack t a -> Just (t,a)
parseInstr _ = Nothing

gNode :: Int -> Int -> Node
gNode t n = NGlobal n [Pack t n, Update 0, Unwind]

pushglobal :: Name -> Transition
pushglobal f st | Just (t,n) <- parseInstr (show f) =
            case aMLookup (getGlobals st) f of
                    Just a -> -- (3.37)
                        putStack (a : getStack st) st
                    Nothing -> --3.38
                        let
                            (h',a) = hAlloc (getHeap st) (gNode t n)
                            glbs = aAssoc (getGlobals st) f a
                            stk = getStack st
                        in
                            putGlobals glbs $
                            putHeap h' $
                            putStack (a:stk) st
                | otherwise = -- (3.5)
    let
        a = search_Gbl f (getGlobals st) ("Undeclared globals:" ++ (show f))
        nstk = a : getStack st
    in putStack nstk st

pushint :: Int -> Transition -- X (3.6)
pushint i st = case aMLookup (getGlobals st) (toText $ show i) of
                Just a -> putStack (a : getStack st) st -- (3.13)
                Nothing -> -- (3.14)
                    let
                        (h',a) = hAlloc (getHeap st) (NNum i)
                        nstk = a : getStack st
                        nglbs = aAssoc (getGlobals st) (toText $ show i) a
                    in putGlobals nglbs $ putStack nstk $ putHeap h' st

mkap :: Transition -- (3.7)
mkap st =
    let
        (a1:a2:as) = getStack st
        (h',a) = hAlloc (getHeap st) (NAp a1 a2)
    in putStack (a:as) $ putHeap h' st

push :: Int -> Transition -- X (3.8) | (3.18)
push n st =
    let
        as = getStack st
        a = as !! n
    in putStack (a:as) st

slide :: Int -> Transition -- (3.9)
slide n st =
    let (a:as) = getStack st
    in putStack (a: drop n as) st

unwind :: Transition
unwind st =
    newState (hLookup (getHeap st) a)
    where
        (a:as) = getStack st
        (glb,lcl) = st
        newState (NConstr t xs) | null (getCode st) = if not $ null $ getDump st
                then    -- (3.35)
                    let
                        ((i',s',v') : d) = getDump st
                    in putVStack v' $ putCode i' $ putStack (a:s') st
                else error "null dump?"
        newState (NNum n) | null (getCode st) = if not $ null $ getDump st
                                                then -- (3.22)
                                                    let ((i',s',v'):d) = getDump st
                                                    in  putVStack v' $
                                                        putDump d $
                                                        putStack (a:s') $
                                                        putCode i' st
                                                else st -- (3.10)
        newState (NAp a1 a2) = putCode [Unwind]
                            $ putStack (a1:a:as)
                            $ lock a st -- (3.11)
        newState (NInd a1) = putCode [Unwind] $ putStack (a1:as) st -- (3.17)
        newState (NGlobal n c) | length as < n =  -- (3.29)
                if null $ getDump st then error "Empty dump unwind."
                else
                    let ((i,s,v):d) =  getDump st
                        ak = last as
                in putVStack v $ putDump d $ putCode i $ putStack (ak:s) st
                               | otherwise =
                    let
                        st' = putCode c $ putStack (rearrange n (getHeap st) (a:as)) st
                    in
                        if n == 0 then lock a st' else st'
        newState (NLAp a1 a2 pl) = putHeap (hUpdate (getHeap st) a (NLAp a1 a2 (lcl : pl))) $ (glb,emptyTask)
        newState (NLGlobal 0 c pl) = putHeap (hUpdate (getHeap st) a (NLGlobal 0 c (lcl:pl))) $ (glb, emptyTask)
        newState e = error $ "newState"++ show a  ++" @" ++ (show e) ++ "State:\n" ++ show st

rearrange :: Int -> GmHeap -> GmStack -> GmStack
rearrange n heap as =
        take n as' ++ drop n as
    where
        as' = map (getArg . hLookup heap) (tail as)

lock :: Addr -> GmState -> GmState
lock addr st =
    putHeap (newHeap (hLookup heap addr)) st
    where
        heap = getHeap st
        newHeap (NAp a1 a2) = hUpdate heap addr (NLAp a1 a2 [])
        newHeap (NGlobal n c)   | n == 0 = hUpdate heap addr (NLGlobal n c [])
                                | otherwise = heap

unlock :: Addr -> GmState -> GmState
unlock addr state =
    newState (hLookup heap addr)
    where
        heap = getHeap state
        newState (NLAp a1 a2 pdl) = emptyPendingList pdl $
                                    unlock a1 $ putHeap (hUpdate heap addr (NAp a1 a2)) state
        newState (NLGlobal n c pdl) = emptyPendingList pdl $
                                    putHeap (hUpdate heap addr (NGlobal n c)) state
        newState _ = state

emptyPendingList :: [PgmLocalState] -> GmState -> GmState
emptyPendingList  tasks state = putSparks (tasks ++ getSparks state) state

-- Compilation

compile :: CoreProgram -> PgmState
compile program = (PgmState [] heap globals [] [], [initialTask addr])
    where
        (heap, globals) = buildInitialHeap program
        addr = aLookupWithErr globals (toText "main") "Main undefined!"

initialTask :: Addr -> PgmLocalState
initialTask addr = PgmLState initialCode [addr] [] [] 0

buildInitialHeap :: CoreProgram -> (GmHeap, GmGlobals)
buildInitialHeap program =
    foldl allocateSc (hInitial,aEmpty) compiled
    where compiled = map compileSc $ program ++ preludeDefs ++ primitives

allocateSc :: (GmHeap, GmGlobals) -> GmCompiledSC -> (GmHeap, GmGlobals)
allocateSc (oh,as) (nm, narg, instns) =
    (h', aAssoc as nm addr)
    where
        (h',addr) = hAlloc oh (NGlobal narg instns)

initialCode :: GmCode
initialCode = [Eval, Print]

data LastInstr = SLide | POP | Noth

-- CoreScDefn = (Name, [Name], Expr Name)
compileSc :: CoreScDefn -> GmCompiledSC
compileSc (nm, args, bd) = let ln = length args in
    (nm, ln, compileR ln bd (afromList $ zip args [0..]))

compileR :: Int -> GmCompiler
compileR d (ELet rec bds e) p | not rec = compileLet (compileR (length bds + d)) Noth bds e p
                              | rec = compileLetrec (compileR (length bds + d)) Noth bds e p
compileR d (EAp (EAp (EVar op) e1) e2) p | op == (toText "par") =
        compileC e2 p ++ [Push 0 ,Par] ++
        compileC e1 (argOffset 1 p) ++ [Mkap, Update d, Pop d, Unwind]
compileR d (EAp (EAp (EAp (EVar si) e0 ) e1) e2) p | si == (toText "if") =
        compileB e0 p ++
        [Cond (compileR d e1 p) (compileR d e2 p)]
compileR d (ECase e alts) p = compileE e p ++ [Casejump (compileDR d alts p)]
compileR d e env | (ELam _ _) <- e = compileE e env ++ [Update d, Pop d, Return] -- Not so sure about this one
                 | (ENum _ ) <- e = compileE e env ++ [Update d, Pop d, Return]
                 | (EConstr _ _) <- e = compileE e env ++ [Update d, Pop d, Return]
                 | otherwise = compileE e env ++ [Update d, Pop d, Unwind]

lastAp :: CoreExpr -> (CoreExpr, [CoreExpr])
lastAp (EAp e1 e2) = let (e',xs) = lastAp e1 in (e',e2:xs)
lastAp e = (e,[])

compileE :: GmCompiler
compileE (ENum i) p = [Pushint i]
compileE (ELet rec defs e) p | rec = compileLetrec compileE SLide defs e p
                             | otherwise = compileLet compileE SLide defs e p
compileE (EAp (EVar n) e) p | n == (toText "negate") = compileB e p ++ [Mkint]
-- Here we give it to compileB
-- compileE (EAp (EAp (EVar o) e1) e2) p | Just op <- aMLookup builtInDyadic o =
--                             compileE e2 p ++
--                             compileE e1 (argOffset 1 p) ++
--                             [op]
compileE (EAp (EAp (EVar op) e1) e2) p | op == (toText "par") =
        compileC e2 p ++ [Push 0, Par] ++
        compileC e1 (argOffset 1 p) ++ [Mkap, Eval]
compileE e@(EAp (EAp (EVar o) e1) e2) p | Just eop <- aMLookup builtInDyadic o =
                            compileB e p ++ (
                            case eop of
                                Left _ -> [Mkint]
                                Right _ -> [Mkbool])
compileE (EAp (EAp (EAp (EVar si) e0 ) e1) e2) p | si == (toText "if") =
    compileB e0 p ++ [Cond (compileE e1 p) (compileE e2 p)]
compileE (ECase e alts) p = compileE e p ++ [Casejump (compileD alts p)]
compileE e p | (EConstr t a, xs) <- lastAp e =
    if length xs /= a then
            error "WaT? Compilee with bad pack?"
    else
        (fst $ foldl (\(xs,p') x -> (compileC x p' ++ xs,argOffset 1 p)) ([],p) xs) ++ [Pack t a]
compileE e p = compileC e p ++ [Eval]

compileB :: GmCompiler
compileB (ENum i) p = [Pushbasic i]
compileB (ELet rec defs e) p | rec = compileLetrec compileB POP defs e p
                             | otherwise = compileLet compileB POP defs e p
compileB (EAp (EAp (EVar o) e1) e2) p | Just op <- aMLookup builtInDyadic o =
                            compileB e2 p ++
                            compileB e1 p ++
                            [either id id op]
compileB (EAp (EVar s) e) p | toText "negate" == s = compileB e p ++ [Neg]
compileB (EAp (EAp (EAp (EVar si) e0 ) e1) e2) p | si == (toText "if") =
    compileB e0 p ++ [Cond (compileB e1 p) (compileB e2 p)]
compileB e p = compileE e p ++ [Get]

compileC :: GmCompiler
compileC (EVar v) env   | aInDom env v =
            let n = aLookupWithErr env v "Can't happend" in [Push n]
                        | otherwise = [Pushglobal v]
compileC (ENum n) _ = [Pushint n]
compileC (EAp e1 e2) env =  compileC e2 env ++
                            compileC e1 (argOffset 1 env) ++
                            [Mkap]
compileC (ELet rec defs e) env | rec = compileLetrec compileC SLide defs e env
                               | otherwise = compileLet compileC SLide defs e env
compileC (EConstr t a) p = [Pushglobal $ toText $ "Pack "++show t ++ " " ++ show a]

argOffset :: Int -> GmEnvironment -> GmEnvironment
argOffset i m = aMap (+i) m

preprosprim :: [(String, [String], CoreStr)] -> CoreProgram
preprosprim xs = map (\(nm,arg,bd) -> (toText nm, map toText arg, fmap toText bd)) xs

primitives = preprosprim primitives'

primitives' =
    [("+",["x","y"], [expr|x+y|])
    ,("-",["x","y"], [expr|x-y|])
    ,("*",["x","y"], [expr|x*y|])
    ,("div",["x","y"], [expr|div x y|])
    -- Negation
    ,("negate",["x"], [expr|negate x|])
    -- Comps
    ,("==",["x","y"], [expr|x == y|])
    ,("/=",["x","y"], [expr|x /= y|])
    ,(">=",["x","y"], [expr|x >= y|])
    ,(">",["x","y"], [expr|x > y|])
    ,("<=",["x","y"], [expr|x <= y|])
    ,("<",["x","y"], [expr|x < y|])
    -- conds
    ,("if",["c","t","f"], [expr|if c t f|])
    ,("True",[], EConstr 2 0)
    ,("False",[], EConstr 1 0)
    ,("par",["x","y"],[expr| par x y|])
    ]

builtInDyadic :: ASSOC Name (Either Instruction Instruction)
builtInDyadic = afromList $ map (\(a,b) -> (toText a, b))
                    [("+",Left Add),("-",Left Sub),("*",Left Mul),("div",Left Div)
                    ,("==",Right Eq),("/=",Right Ne),(">=",Right Ge),(">",Right Gt)
                    ,("<",Right Lt),("<=",Right Le)]

compileLet :: GmCompiler -- Esto es para no modificarlo desp.
        -> LastInstr
     -> [(Name, CoreExpr)] -> CoreExpr -> GmEnvironment -> GmCode
compileLet comp last defs expr env =
        compileLet' defs env ++
        comp expr env' ++
            case last of
                SLide -> [Slide (length defs)]
                POP -> [Pop (length defs)]
                Noth -> []
    where
        env' = compileArg defs env

compileLet' :: [(Name,CoreExpr)] -> GmEnvironment -> GmCode
compileLet' [] _ = []
compileLet' (d:defs) env = compileC (snd d) env ++ (compileLet' defs (argOffset 1 env))

compileArg :: [(Name,CoreExpr)] -> GmEnvironment -> GmEnvironment -- ro'
compileArg defs env = aConcat (afromList $ zip (map fst defs) [(n-1)..0]) (argOffset n env)
    where
        n = length defs

compileLetrec :: GmCompiler
        -> LastInstr
        -> [(Name, CoreExpr)] -> CoreExpr -> GmEnvironment -> GmCode
compileLetrec comp last defs expr env = Alloc ln : eps ++ comp expr env' ++
            case last of
                SLide -> [Slide ln]
                POP -> [Pop ln]
                Noth -> []
    where
        ln = length defs
        env' = compileArg defs env
        eps = fst $ foldr (\(_,e) (es, n) ->
                        let n' = n - 1 in (comp e env' ++ [Update n'] ++ es, n')) ([],ln) defs

compileD :: [CoreAlt] -> GmEnvironment -> [(Int,GmCode)]
compileD xs p = compileAlts compileE' xs p

compileDR :: Int -> [CoreAlt] -> GmEnvironment -> [(Int,GmCode)]
compileDR d xs p = compileAlts (compileR' d) xs p

compileAlts :: (Int -> GmCompiler) -- compiler for alternative bodies
        -> [CoreAlt] -- the list of alternatives
        -> GmEnvironment -- the current environment
        -> [(Int,GmCode)] -- list of alternative code seuqneces
compileAlts comp alts env =
   [ (tag, comp (length names) body (aConcat (afromList$ zip names [0..]) (argOffset (length names) env)))
         | (tag, names, body) <- alts]

compileE' :: Int -> GmCompiler
compileE' offset expr env = [Split offset] ++ compileE expr env ++ [Slide offset]

compileR' :: Int -> Int -> GmCompiler
compileR' d offset expr env = [Split offset] ++ compileR (d + offset) expr env ++ [Slide offset]

opTextView :: Name -> String
opTextView = show
