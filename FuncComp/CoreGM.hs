module CoreGM where

import           CoreLanguage
import           CoreUtils

data GmState = GmSt {gmCode :: GmCode , -- Current instr stream
                gmStack     :: GmStack , -- Current stack
                gmDump      :: GmDump, -- current Dump
                gmVStack    :: GmVStack, -- Current V-Stack
                gmHeap      :: GmHeap , -- Heap of nodes
                gmGlobals   :: GmGlobals , -- Global addresses in heap
                gmOutput    :: GmOutput, -- Current output
                gmStats     :: GmStats } -- Statistics

type GmVStack = [Int]

initialVStack :: GmVStack
initialVStack = []

emptyVStack :: GmVStack
emptyVStack = []

getVStack :: GmState -> GmVStack
getVStack = gmVStack

putVStack :: GmVStack -> GmState -> GmState
putVStack stk st = st{gmVStack = stk}


type GmOutput = [(Int,[String])]

initialOutput :: GmOutput
initialOutput = [(0,["FF"])]

putNumber :: String -> GmOutput -> GmOutput
putNumber num [] = [(0,['#':num])]
putNumber num r@((n,sat):out)   | n > 0 = (n-1, num : sat) : out
                                | otherwise = (0,['#':num]) : r

putConstr :: Int -> Int -> GmOutput -> GmOutput
putConstr t n xs = (n,["Pack" ++ show t]) : xs

getOutput :: GmState -> GmOutput
getOutput = gmOutput

putOutput :: GmOutput -> GmState -> GmState
putOutput s st = st{gmOutput = s}

type GmDump = [GmDumpItem]
type GmDumpItem = (GmCode, GmStack, GmVStack)

initialDump :: GmDump
initialDump = []

getDump :: GmState -> GmDump
getDump = gmDump

putDump :: GmDump -> GmState -> GmState
putDump d st = st{gmDump = d}

instance Show GmState where
    show (GmSt code stack dump vstack heap globals o stats) =
        "Code: " ++ show code ++ "\n" ++
        "Stack: " ++ show stack ++ "\n" ++
        "Heap : " ++ show heap ++ "\n" ++
        "Globals : " ++ show globals++ "\n" ++
        "Stats : " ++ show stats++ "\n" ++
        "Dump : " ++ show dump ++ "\n" ++
        "Output : " ++ show o ++"\n" ++
        "VStack : " ++ show vstack

type GmCode = [Instruction]

getCode :: GmState -> GmCode
getCode = gmCode

putCode :: GmCode -> GmState -> GmState
putCode c st = st{gmCode = c}

data Instruction
    = Unwind
    | Pushglobal Name
    | Pushint Int
    | Push Int
    | Mkap
    | Update Int
    | Pop Int
    | Slide Int -- Mark3
    | Alloc Int
-- Mark4
    | Eval
    | Add | Sub | Mul | Div | Neg
    | Eq | Ne | Lt | Le | Gt | Ge
    | Cond GmCode GmCode
-- Mark 5
    | Pack Int Int
    | Casejump [(Int, GmCode)]
    | Split Int
    | Print
-- Mark 7
    | Pushbasic Int
    | Mkbool | Mkint
    | Get
    | Return --Ex 3.46
    deriving (Eq, Show,Read)

-- GmStack
--
type GmStack = [Addr]
push_Stack = (:)
top_Stack = head
pop_Stack = tail

getStack :: GmState -> GmStack
getStack = gmStack

putStack :: GmStack -> GmState -> GmState
putStack sk st = st{gmStack = sk}

-- GmHeap
--
type GmHeap = Heap Node

getHeap :: GmState -> GmHeap
getHeap = gmHeap

putHeap :: GmHeap -> GmState -> GmState
putHeap h st = st{gmHeap = h}

data Node
    = NNum Int -- Numbers
    | NAp Addr Addr -- Applications
    | NGlobal Int GmCode -- Globals
    | NInd Addr -- Indirections
    | NConstr Int [Addr] -- Constructor
    deriving (Show,Eq)

getArg :: Node -> Addr
getArg (NAp _ a) = a
getArg _ = error "getArg err"
-- GmGlobals
--
type GmGlobals = ASSOC Name Addr

search_Gbl :: Name -> GmGlobals -> String -> Addr
search_Gbl nm gbl str = aLookupWithErr gbl nm str

getGlobals :: GmState -> GmGlobals
getGlobals = gmGlobals

putGlobals :: GmGlobals -> GmState -> GmState
putGlobals gg st = st{gmGlobals = gg}
--
type GmStats = Int
statInitial :: GmStats
statInitial  = 0
statIncSteps :: GmStats -> GmStats
statIncSteps = (+1)
statGetSteps :: GmStats -> Int
statGetSteps = id

gmStIncSteps :: GmState -> GmState
gmStIncSteps s = putStats (statIncSteps $ getStats s) s

getStats :: GmState -> GmStats
getStats = gmStats

putStats :: GmStats -> GmState -> GmState
putStats ta st = st{gmStats = ta}
----

----
-- SC compilation
----
type GmCompiledSC = (Name, Int, GmCode)

type GmCompiler = CoreExpr -> GmEnvironment -> GmCode
type GmEnvironment = ASSOC Name Int -- Name, offset
