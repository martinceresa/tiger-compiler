{-# LANGUAGE QuasiQuotes #-}

import           CoreLambda
import           CoreLanguage
import           CorePP
import           CoreQuoter

test = [program| main = let g = \ x . x in g 3 |]

test1 = [program| main = letrec pe = g 23; ja = \ x . x; sa = ja 2 in sa; g x = 2 * x |]

test2 = [program| main = g 2 3; g x b = x + b|]

testintro = [program|main=f 6; f x = letrec g = \ y . cons (x*y) (g y) in g 3|]

ex642 = [program|main=2;f = \ x . x + 1|]

test65 = [program|main = f 6;f x = letrec g = \ y . cons (x*y) (g y) in g 3|]

test643 = [program|main = 2; f x = let g = (\ y . y + 1) in g (g x) |]

test653 = [program|main = 1; f x y = letrec
                                        g = \ p . h p + x;
                                        h = \ q . k + y + q;
                                        k = g y in g 4|]
