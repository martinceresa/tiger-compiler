module CoreParser where

import           Text.Parsec
import           Text.Parsec.Combinator

import qualified Text.Parsec.Expr       as Ex
import qualified Text.Parsec.Token      as Tok

-- import           Data.Text              hiding (map)

import           CoreLanguage
import           CoreLexer

binary s f = Ex.Infix (reservedOp s >> return (\e1 e2 -> EAp (EAp f e1) e2))

evar = EVar . toText

table = [
            [binary "*" (evar $ "*") Ex.AssocLeft
            ,binary "/" (evar $ "/") Ex.AssocLeft]
            ,
            [binary "+" (evar $ "+") Ex.AssocLeft
            ,binary "-" (evar $ "-") Ex.AssocLeft]
            ,
            [binary "==" (evar $ "==") Ex.AssocNone
            ,binary "/=" (evar $ "~=") Ex.AssocNone
            ,binary ">" (evar $">") Ex.AssocNone
            ,binary "<=" (evar $ "<=") Ex.AssocNone
            ,binary ">=" (evar $ ">=") Ex.AssocNone
            ,binary "<" (evar $ "<") Ex.AssocNone]
        ]

int = ENum <$> number

var = evar <$> identifier

alt = do
    symbol "<"
    n <- number
    symbol ">"
    ts <- many identifier
    symbol "->"
    e <- expr
    return (n, ts, e)

alts = sepBy1 alt (symbol ";")

dfn = do
    v <- identifier
    symbol "="
    e <- expr
    return (v,e)

dfns = sepBy1 dfn $ symbol ";"

sc = do
    v <- identifier
    vs <- many identifier
    symbol "="
    e <- expr
    return (v, vs, e)

programs = sepBy1 sc (symbol ";")

constr = do
    reserved "Pack"
    symbol "{"
    n <- number
    symbol ","
    m <- number
    symbol "}"
    return $ EConstr n m

aexpr =
    var <|> int <|> constr <|> parens expr

letexp = do
    reserved "let"
    dfs <- dfns
    reserved "in"
    e <- expr
    return $ ELet nonrecursive dfs e

letrecexp = do
    reserved "letrec"
    dfs <- dfns
    reserved "in"
    e <- expr
    return $ ELet recursive dfs e

caseexp = do
    reserved "case"
    e <- expr
    reserved "of"
    as <- alts
    return $ ECase e as

lambda = do
    reserved "\\"
    as <- many1 identifier
    dot
    e <- expr
    return $ ELam as e


eap =
    return EAp

expr' = chainl1 aexpr eap <|>
        letexp <|> letrecexp <|> caseexp
        <|> lambda

expr = Ex.buildExpressionParser table expr'
