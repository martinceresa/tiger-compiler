module CoreTim where

import           CoreLanguage
import           CoreUtils

data Instruction = Take Int Int -- Mark 3 modification
                | Enter TimAMode
                | Push TimAMode
                -- Mark 2 arithmetics
                | Return
                | PushV ValueAMode
                | Op Ops
                -- | Cond [Instruction] [Instruction]
                -- Mark 3 letrec
                | Move Int TimAMode
                -- Mark 4
                | PushMarker Int
                | UpdateMarkers Int
                -- Mark 5
                | ReturnConstr Int
                | Switch [[Instruction]]
                | Print
    deriving Show

data Ops =   Add | Sub | Mult | Div | Neg
            | Gr | GrEq | Lt | LtEq | Eq | NotEq
    deriving (Eq,Show)

opes =afromList $ map (\(a,b) -> (toText a,b)) [("+",Add),("*",Mult),("div",Div),("<",Lt)
                 ,("-",Sub),(">",Gr),(">=",GrEq),("<=",LtEq),("==",Eq)
                ,("/=",NotEq)]

isOp = aInDom opes

opsTtoO :: Name -> Ops
opsTtoO s = aLookupWithErr opes s ("Internal error 123"++ show s)

data ValueAMode = Fp | IntVConst Int
    deriving Show

data TimAMode = Arg Int
                | Label Name Int
                | Code [Instruction]
                | IntConst Int
                | Data Int
    deriving Show

data TimState = TimState
                { instr  :: [Instruction]
                , fp     :: FramePtr
                , dfp    :: FramePtr
                , stack  :: TimStack
                , vstack :: TimValueStack
                , dump   :: TimDump
                , heap   :: TimHeap
                , store  :: CodeStore
                , stats  :: TimStats
                , output :: [Int]
                }

instance Show TimState where
    show (TimState ins fp dfp stack vstack dump heap sote stats output) =
        --"Instructions: " ++ show ins ++ "\n" ++
        -- "FP: " ++ show fp++ "\n" ++
        -- "DFP: " ++ show dfp++ "\n" ++
        -- "Stack: " ++ show stack ++ "\n" ++
        "VStack: " ++ show vstack ++ "\n" ++
        -- "Dump: " ++ show dump++ "\n" ++
        -- "Heap: " ++ show heap ++ "\n" ++
        -- "Heap Size: " ++ show (heapStats heap) ++ "\n" ++
        -- "Store: " ++ show sote++ "\n" ++
        "Stats: " ++ showStats stats++ "\n" ++
        "Output : " ++ show output++ "\n"

data FramePtr = FrameAddr Addr -- The adress of a frame
                | FrameInt Int
                | FrameNull
    deriving Show

getFpInt :: FramePtr -> Int
getFpInt (FrameInt n) = n
getFpInt _ = error "No integer in the fp"

getFpAddr :: FramePtr -> Addr
getFpAddr (FrameAddr n) = n
getFpAddr _ = error "No fp addrs"

type TimStack = [Closure]

initialArgStack :: TimStack
initialArgStack = [initialClosure]

type Closure = Maybe ([Instruction], FramePtr)

initialClosure :: Closure
initialClosure = Just ([],FrameNull)

topCont = [Switch
            [[]
            ,[ Move 1 (Data 1) -- Head
             , Move 2 (Data 2) -- Tail
             , Push $ Code headCont
             , Enter (Arg 1)
            ]]]

headCont = [Print, Push (Code topCont), Enter (Arg 2)]

getClr :: Closure -> ([Instruction],FramePtr)
getClr (Just a) = a
getClr _ = error "Empty closure"

mkClosure = Just

genEmpty :: Int -> [Closure]
genEmpty n | n < 0 = error "Take t n, with t < n"
genEmpty m = map (const Nothing) [1..m]

type TimValueStack = [Int] -- Unboxed Integers

initialValueStack :: TimValueStack
initialValueStack = []

type TimDump = [(FramePtr -- Frame to be updated
                ,Int -- Index of slot to be updated
                ,TimStack)] -- Old stack

initialDump :: TimDump
initialDump = []

type TimHeap = Heap Frame

type Frame = [Closure]

fAlloc :: TimHeap -> [Closure] -> (TimHeap , FramePtr)
fAlloc h xs = (heap' , FrameAddr addr)
    where
        (heap' , addr) = hAlloc h xs

fGet :: TimHeap -> FramePtr -> Int -> Closure
fGet h (FrameAddr addr) i = f !! (i - 1)
    where
        f = hLookup h addr
fGet _ e _ = error $ "fGet with " ++ show e

fUpdate :: TimHeap -> FramePtr -> Int -> Closure -> TimHeap
fUpdate h (FrameAddr addr) n cls = hUpdate h addr nfp
    where
        frame = hLookup h addr
        nfp = take (n-1) frame ++ [cls] ++ drop n frame
fUpdate _ e _ _ = error "No Addres in the fp!"

fList :: Frame -> [Closure]
fList f = f

--type CodeStore = (ASSOC Name [Instruction])
-- Mark 6
--type CodeStore = (FramePtr-- Global fp
                --,ASSOC Name Int)
type CodeStore = FramePtr


{-
   codeLookup :: CodeStore -> Name -> [Instruction]
   codeLookup cd nm = case aMLookup cd nm of
                           Just o -> o
                           Nothing -> error "Attempt to jump to unknown label"
-}

type TimStats = (Int,Int)

showStats :: TimStats -> String
showStats (ticks, mstack) =
    "Ticks : " ++ show ticks ++ " || " ++
    "Max Stack: " ++ show mstack

statInitial :: TimStats
statInitial = (0,0)

statIncnSteps :: Int -> TimStats -> TimStats
statIncnSteps n (i,s) = (i + n,s)

statIncSteps :: TimStats -> TimStats
statIncSteps = statIncnSteps 1

stackSizeC :: Int -> TimStats -> TimStats
stackSizeC m (i,n) = (i, max m n)

statGetSteps :: TimStats -> Int
statGetSteps (i,s) = i

heapStats :: TimHeap -> Int
heapStats h = hSize h

-- Nothing so far
intCode :: [Instruction]
intCode = [PushV Fp, Return]
