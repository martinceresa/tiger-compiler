module CoreLexer where

import qualified Data.Text         as T

import           Text.Parsec
import           Text.Parsec.Text
import qualified Text.Parsec.Token as Tok

lexer :: Tok.TokenParser ()
lexer = Tok.makeTokenParser Tok.LanguageDef
    { Tok.commentLine = "--"
    , Tok.commentStart = "{-"
    , Tok.commentEnd = "-}"
    , Tok.nestedComments = False
    , Tok.identStart = letter
    , Tok.identLetter = alphaNum <|> char '_'
    , Tok.opStart = oneOf "+-*/<=~>&|\\"
    , Tok.opLetter = char '='
    , Tok.reservedNames = ["let","letrec","in","case","of","Pack","\\"]
    , Tok.reservedOpNames = ["==","+","-","*","/","<","<=","~=",">=",">","&","|","->"]
    , Tok.caseSensitive = True
    }

reservedOp = Tok.reservedOp lexer
reserved = Tok.reserved lexer

toInt :: Integer -> Int
toInt = fromInteger
number = toInt <$> Tok.natural lexer

parens = Tok.parens lexer
commaSep = Tok.commaSep lexer
commaSep1 = Tok.commaSep1 lexer
semiSep = Tok.semiSep lexer
semiSep1 = Tok.semiSep1 lexer
identifier = Tok.identifier lexer
dot = Tok.dot lexer
colon = Tok.colon lexer
brackets = Tok.brackets lexer
braces = Tok.braces lexer
symbol = Tok.symbol lexer
stringLiteral = Tok.stringLiteral lexer
whiteSpace = Tok.whiteSpace lexer
