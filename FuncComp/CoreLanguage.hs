module CoreLanguage where

import           Data.Text hiding (map)

type Name = Text

toText :: String -> Text
toText = pack

type Alter a = (Int, [a], Expr a)
type CoreAlt = Alter Name

data Expr a =
    EVar Name
    | ENum Int
    | EConstr Int Int
    | EAp (Expr a) (Expr a)
    | ELet IsRec [(a,Expr a)] (Expr a)
    | ECase (Expr a) [Alter a]
    | ELam [a] (Expr a)
    deriving (Show,Eq)

instance Functor Expr where
    fmap f (EVar n) = EVar n
    fmap f (ENum i) = ENum i
    fmap f (EConstr i l) = EConstr i l
    fmap f (EAp l r) = EAp (fmap f l) (fmap f r)
    fmap f (ELet b bs e) = ELet b (map (\(n,b) -> (f n, fmap f b)) bs) (fmap f e)
    fmap f (ECase e cs) = ECase (fmap f e) (map (\(i,ns,e) -> (i, map f ns, fmap f e)) cs)
    fmap f (ELam as e) = ELam (map f as) (fmap f e)

type CoreExpr = Expr Name
type CoreStr = Expr String

type IsRec = Bool

recursive,nonrecursive :: IsRec
recursive = True
nonrecursive = False

bindersOf :: [(a,b)] -> [a]
bindersOf = map fst

rhssOf :: [(a,b)] -> [b]
rhssOf = map snd

isAtomicExpr :: Expr a -> Bool
isAtomicExpr (EVar _) = True
isAtomicExpr (ENum _) = True
isAtomicExpr _ = False

type Program a = [ScDefn a]
type CoreProgram = Program Name

type ScDefn a = (Name, [a], Expr a)
type CoreScDefn = ScDefn Name
type ScStr a = (a,[a],Expr a)
type CoreScDefnStr = ScStr String

type ProgramStr = [CoreScDefnStr]

coreStrToTex :: CoreScDefnStr -> CoreScDefn
coreStrToTex (nm,bds,bd) = (toText nm, map toText bds, fmap toText bd)

--- Annotated
--
type AnnExpr a b = (b, AnnExpr' a b)

data AnnExpr' a b =   AVar Name
                    | ANum Int
                    | AConstr Int Int
                    | AAp (AnnExpr a b) (AnnExpr a b)
                    | ALet Bool [AnnDefn a b ] (AnnExpr a b)
                    | ACase (AnnExpr a b) [AnnAlt a b]
                    | ALam [a] (AnnExpr a b)
    deriving Show

type AnnDefn a b = (a, AnnExpr a b)
type AnnAlt a b = (Int, [a], AnnExpr a b)
type AnnProgram a b = [(Name, [a], AnnExpr a b)]
