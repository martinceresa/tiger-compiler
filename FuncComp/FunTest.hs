{-# LANGUAGE QuasiQuotes #-}
import           CoreEval
import           CoreLanguage
import           CorePrint
import           CoreQuoter
--
-- import           Data.Text

import           Control.Monad

test_b :: CoreProgram
test_b = [(toText "main", [], ENum 1)]

test_2_4 = [(toText "main", [], EAp (EAp (EAp (EVar (toText "S")) (EVar (toText "K"))) (EVar (toText "K"))) (ENum 3))]

test_2_3 = [(toText "main", [], (EAp (EAp (EVar (toText "S")) (EVar (toText "K"))) (EVar (toText "K"))))]

test_let = [(toText "main", [], ELet nonrecursive [(toText "x",ENum 13)] (EAp (EAp (EAp (EVar (toText "S")) (EVar (toText "K"))) (EVar (toText "K"))) (EVar (toText "x"))))]

test_letrec = [(toText "main", [], ELet recursive
        [(toText "y", EVar (toText "x")),(toText "x",EVar (toText "y"))]
        (EAp (EAp (EAp (EVar (toText "S")) (EVar (toText "K"))) (EVar (toText "K"))) (ENum 3)))]

test_letrecposta =
    [(toText "pair", [toText "x",toText "y",toText "f"]
        , EAp (EAp (EVar $ toText "f") (EVar $ toText "x")) (EVar $ toText "y"))
    ,(toText "fst", [toText "p"]
        , EAp (EVar $ toText "p") (EVar $ toText "K"))
    ,(toText "snd", [toText "p"]
        , EAp (EVar $ toText "p") (EVar $ toText "K1"))
    ,(toText "f", [toText "x", toText "y"]
        , ELet recursive [(toText "a", EAp (EAp (EVar $ toText "pair") (EVar $ toText "x")) (EVar $ toText "b")),
                          (toText "b", EAp (EAp (EVar $ toText "pair") (EVar $ toText "y")) (EVar $ toText "a"))]
                 (EAp (EVar $ toText "fst") (EAp (EVar $ toText "snd") (EAp (EVar $ toText "snd") (EAp (EVar $ toText "snd") (EVar $ toText "a"))))))
    ,(toText "main", [], EAp (EAp (EVar $ toText "f") (ENum 3)) (ENum 4))
    ]

nonfinish :: CoreProgram
nonfinish = [(toText "main", [], ELet recursive [(toText "f",EAp (EVar $ toText "f") (EVar $ toText "x"))] (EVar $ toText "f"))]

-- evar :: String ->
evar = EVar . toText

exercise_2_13 :: CoreProgram
exercise_2_13 = [(toText "id", [toText "x"], EVar $ toText "x")
                , (toText "main", [], EAp (EAp (EAp (evar "twice") (evar "twice")) (EVar $ toText "id")) (ENum 3))]

neg :: CoreProgram
neg = [(toText "main", [], EAp (evar "negate") (ENum 3))]

negtwice :: CoreProgram
negtwice = [(toText "main", [], EAp (EAp (evar "twice") (evar "negate")) (ENum 3))]

negttwice = [(toText "main", [], EAp (evar "negate") (EAp (evar "negate") (ENum 3)))]

negind :: CoreProgram
negind = [(toText "main", [], EAp (evar "negate") (EAp (evar "I") (ENum 3)))]

plus :: CoreProgram
plus = [(toText "main", [], EAp (EAp (evar "*") (EAp (evar "I") (ENum 2))) (EAp (evar "I") (ENum 3)))]

sub :: CoreProgram
sub = [(toText "main", [], EAp (EAp (evar "-") (ENum 2)) (ENum 3))]

andcheck1 :: CoreProgram
andcheck1 = [(toText "main",[], EAp (EAp (evar "and") (evar "true")) (evar "false"))]
andcheck2 :: CoreProgram
andcheck2 = [(toText "main",[], EAp (EAp (evar "and") (evar "true")) (evar "true"))]

facto :: CoreProgram
facto =[(toText "fact",[toText "n"], fmap toText [expr|(n == 0) 1 (n * (fact (n-1)))|])
        ,(toText "main", [], fmap toText [expr| fact 15|])]


spairs = [(toText "main",[], EAp (EAp (evar "MkPair") (ENum 1)) (ENum 2) )]
pairs :: CoreProgram
pairs = [(toText "main",[], EAp (evar "fst") (EAp (evar "snd") (EAp (evar "fst") (EAp (EAp (evar "MkPair") (EAp (EAp (evar "MkPair") (ENum 1)) (EAp (EAp (evar "MkPair") (ENum 2)) (ENum 3)))) (ENum 4)))))]

primer = [(toText "main",[], EAp (evar "fst") (EAp (EAp (evar "MkPair") (ENum 1)) (ENum 2)) )]

segundo = [(toText "main",[], fmap toText [expr|snd (pair 1 2)|])]

lista = [expr|Cons 1 (Cons 2 Nil)|]

long = [("length",["xs"],[expr|caseList xs 0 lengthP|])
        , ("lengthP", ["x","xs"], [expr| 1 + length xs|])]

longList = [("main", [], [expr| length (cons nil (cons (cons 1 nil) nil))|])
            , ("lista",[], lista)] ++ long

execute' l = execute $ [(toText "main",[], fmap toText l)]
executeStr l = execute $ map (\(nm,args,e) -> (toText nm, map toText args, fmap toText e)) l

execute :: CoreProgram -> IO ()
execute = showState . Prelude.last . eval . compile

debug :: CoreProgram -> IO ()
debug s = (mapM_ showDStack) $ eval $ compile s
