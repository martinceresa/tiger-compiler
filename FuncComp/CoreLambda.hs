module CoreLambda where

import           CoreLanguage
import           CorePP
import           CoreUtils

import           Debug.Trace

lambdalift ::   CoreProgram  -- With ELam
            ->  CoreProgram  -- Without ELams
lambdalift = collectSCs . rename . abstract . freeVars

runS :: CoreProgram -> String
runS = ppprog . lambdalift

freeVars :: CoreProgram -> AnnProgram Name (Set Name)
freeVars prog = [ (nm,bds, freeVarse (setFromList bds) expr) | (nm,bds, expr) <- prog]

freeVarse :: (Set Name) -- Candidates for free variables
            -> CoreExpr -- Expression to annotae
            -> AnnExpr Name (Set Name) -- Annotated result
freeVarse lv (ENum k) = (setEmpty, ANum k)
freeVarse lv (EVar x) | setElemOf x lv = (setSingleton x, AVar x)
                      | otherwise = (setEmpty, AVar x)
freeVarse lv (EAp f x) = (setUnion (freeVarsOf f') (freeVarsOf x'), AAp f' x')
    where
        f' = freeVarse lv f
        x' = freeVarse lv x
freeVarse lv (ELam bds bd) = (setSubtraction (freeVarsOf bd') set_bds, ALam bds bd')
    where
        bd' = freeVarse new_lv bd
        new_lv = setUnion lv set_bds
        set_bds = setFromList bds
freeVarse lv (ELet isrec defns body) = (setUnion defnsFree bodyFree , ALet isrec defns' body')
    where
        binders = bindersOf defns
        binderSet = setFromList binders
        body_lv = setUnion lv binderSet
        rhs_lv  | isrec = body_lv
                | otherwise = lv
        rhss' = map (freeVarse rhs_lv) (rhssOf defns)
        defns' = zip binders rhss'
        freeInValues = setUnionList (map freeVarsOf rhss')
        defnsFree    | isrec = setSubtraction freeInValues binderSet
                    | otherwise = freeInValues
        body'  = freeVarse body_lv body
        bodyFree = setSubtraction (freeVarsOf body') binderSet
freeVarse lv (ECase e alts) =  freeVarseCase lv e alts
freeVarse lv (EConstr t a) = error "freeVarse no case for constructors"

freeVarseCase :: Set Name -> CoreExpr -> [CoreAlt] -> AnnExpr Name (Set Name)
freeVarseCase lv c alts =  (setUnion (freeVarsOf c') as, ACase c' alts')
    where
        c' = freeVarse lv c
        (as, alts') = foldr (\a (acc,as) ->
                    let a' = freeVarseAltse lv a in
                        (setUnion acc (freeVarsAltOf a'), a':as)) (setEmpty,[]) alts

freeVarseAltse :: Set Name -> CoreAlt -> AnnAlt Name (Set Name)
freeVarseAltse lv (i, bds, bd) = (i,bds,
                            (setSubtraction (freeVarsOf bd') set_bds , getExpr bd'))
    where
        set_bds = setFromList bds
        new_lv = setUnion lv set_bds
        bd'  = freeVarse new_lv bd :: AnnExpr Name (Set Name)

freeVarsOf :: AnnExpr Name (Set Name) -> Set Name
freeVarsOf = fst

getExpr :: AnnExpr Name (Set Name) -> AnnExpr' Name (Set Name)
getExpr = snd

freeVarsAltOf :: AnnAlt Name (Set Name) -> Set Name
freeVarsAltOf (_,_,(a,_)) =  a

freeVarsOfAlt :: AnnAlt Name (Set Name) -> Set Name
freeVarsOfAlt (tag,arg,rhs) = setSubtraction (freeVarsOf rhs) (setFromList arg)
--
abstract :: AnnProgram Name (Set Name) -> CoreProgram
abstract prog = [ (sc_name, arg, abstracte rhs)| (sc_name, arg, rhs) <- prog]
--
abstracte :: AnnExpr Name (Set Name) -> CoreExpr
abstracte (_, ANum i) = ENum i
abstracte (_, AVar n) = EVar n
abstracte (_, AAp f x) = EAp (abstracte f) (abstracte x)
abstracte (_, ALet isrec defns body) = ELet isrec [ (name, abstracte bd) | (name,bd) <- defns ] (abstracte body)
abstracte (free, ALam args  body) = foldl EAp sc (map EVar fvList)
    where
        fvList = setToList free
        sc = ELet False [(toText "sc", sc_rhs)] (EVar $ toText "sc")
        sc_rhs = ELam (fvList ++ args) (abstracte body)
abstracte (_, AConstr t a) = error "no case for Constr"
abstracte (free, ACase e alts) = abstractCase free e alts

abstractCase :: Set Name -> AnnExpr Name (Set Name) -> [AnnAlt Name (Set Name)] -> CoreExpr
abstractCase free e alts = ECase e' alts'
    where
        e' = abstracte e
        alts' = map (\(i,bds,e) -> (i,bds, abstracte e)) alts

--- REnameing
renameGen :: (NameSupply -> [a] -> (NameSupply, [a], ASSOC Name Name))
            -> Program a
            -> Program a
renameGen nbds prog = snd $ mapAccuml rename_sc initialNameSupply prog
    where
        rename_sc ns (sc_name, args, rhs) = (ns2, (sc_name, args', rhs'))
            where
                (ns1,args', env) = nbds ns args
                (ns2, rhs') = renameGene nbds env ns1 rhs

renameGene :: (NameSupply -> [a] -> (NameSupply, [a], ASSOC Name Name))
            -> ASSOC Name Name
            -> NameSupply
            -> Expr a
            -> (NameSupply, Expr a)
renameGene nbds env ns (EVar v) = (ns, EVar $ aLookup env v v)
renameGene nbds env ns (ENum n) = (ns, ENum n)
renameGene nbds env ns (EAp f x) = (ns2, EAp f' x')
    where
        (ns1, f') = renameGene nbds env ns f
        (ns2, x') = renameGene nbds env ns1 x
renameGene nbds env ns (ELam as bd) = (ns2, bd')
    where
        (ns1,as',env') = nbds ns as
        (ns2, bd') = renameGene nbds env' ns1 bd
renameGene nbds env ns (ELet isrec defns bd) = (ns3, ELet isrec (zip bds' rhss') bd')
    where
        (ns1, bd') = renameGene nbds bd_env ns bd
        bds = bindersOf defns
        (ns2,bds', env') = nbds ns1 bds
        bd_env = aConcat env' env
        (ns3,rhss') = mapAccuml (renameGene nbds rhsEnv) ns2 (rhssOf defns)
        rhsEnv  | isrec = bd_env
                | otherwise = env

renameGene nbds env ns (EConstr t a) = error "renameGene no case for constrs"
-- renameGene nbds env ns (ECase e alts) = renameGene_case nbds env ns e alts

rename :: CoreProgram -> CoreProgram
rename = renameGen newNames

{-
   renameCase :: ASSOC Name Name -> NameSupply -> CoreExpr -> [CoreAlt] -> (NameSupply,CoreExpr)
   renameCase env ns e alts = (ns',ECase e' alts')
       where
           (ns1,e') = renamee env ns e
           (ns',alts') = foldr (\(i,bds,bd) (ns',as) ->
                               let
                                   (ns'', nbds, env') = newNames ns' bds
                                   (_, bd') = renamee (aConcat env' env) ns'' bd
                               in (ns'', (i,nbds,bd') : as)) (ns1,[]) alts
-}
--renameCase _ _ _ _ = error "not implemented yet"

newNames :: NameSupply -> [Name] -> (NameSupply, [Name], ASSOC Name Name)
newNames ns old_names = (ns', new_names, env)
    where
        (ns', new_names) = getNames ns old_names
        env = afromList $ zip old_names new_names


collectSCs :: CoreProgram -> CoreProgram
collectSCs prog = concat (map collect_one_sc prog)
    where
        collect_one_sc (sc_name, bds1,ELet _ [(a,ELam bds2 bd)] (EVar b)) | a == b
                     = (sc_name, bds1 ++ bds2, bd') : scs
            where
                (scs, bd') = collectSCse bd
        collect_one_sc (sc_name, bds, rhs) = (sc_name, bds, rhs') :scs
            where
                (scs, rhs') = collectSCse rhs

collectSCse :: CoreExpr -> ([CoreScDefn], CoreExpr)
collectSCse (ENum n)= ([], ENum n)
collectSCse (EVar v) = ([], EVar v)
collectSCse (EAp f x) = (scsf ++ scsx, EAp rhsf rhsx)
    where
        (scsf, rhsf) =  collectSCse f
        (scsx, rhsx) = collectSCse x
collectSCse (ELam args body) = (scs, ELam args body')
    where
        (scs, body') = collectSCse body
collectSCse (EConstr _ _ ) = error "no case for constructor"
collectSCse (ECase e alts ) = (scse ++ scs_alt, ECase e' alts')
    where
        (scse, e') = collectSCse e
        (scs_alt, alts') = mapAccuml collectSCalts [] alts
        collectSCalts scs (tag,args,bd) = (scs ++ scs', (tag, args, bd'))
                where
                    (scs',bd') = collectSCse bd
collectSCse (ELet isrec defns body) = (rhss_scs ++ body_scs ++ local_scs, mkELet isrec non_scs'  body')
    where
        (rhss_scs, defns') = foldr (\(nm,ce) (rhss,ds) ->
                                case ce of
                                  ELet _ [(y,ELam bds bd)] (EVar x) | y == x ->
                                    let (add, bd') = collectSCse bd in
                                    ((nm,bds,bd') : add ++ rhss,ds)
                                  _ -> collectSCsd ds rhss (nm,ce)) ([],[]) defns
        scs' = [(name,rhs)  | (name, rhs) <- defns', isElam rhs]
        non_scs' = [(name,rhs) | (name, rhs) <- defns', not $ isElam rhs]
        local_scs = [(name,args,body) | (name, ELam args body) <- scs']
        (body_scs, body') = collectSCse body

collectSCsd :: [(Name, CoreExpr)] -> [CoreScDefn] -> (Name, CoreExpr) -> ([CoreScDefn],[(Name, CoreExpr)])
collectSCsd es scs (name, rhs) = (rhs_scs ++ scs, (name, rhs'): es)
        where
            (rhs_scs, rhs') = collectSCse rhs

isElam (ELam {}) = True
isElam  _ = False

mkELet :: IsRec -> [(Name,CoreExpr)] -> CoreExpr -> CoreExpr
mkELet _ [] e = e
mkELet b xs e = ELet b xs e

----- Johnsson Style!
--
lambdaLiftJ :: CoreProgram -> CoreProgram
lambdaLiftJ = collectSCs . abstractJ . freeVars . rename

runJ :: CoreProgram -> String
runJ = ppprog . lambdaLiftJ

abstractJ :: AnnProgram Name (Set Name) -> CoreProgram
abstractJ prog = [ (nm,args, abstractJe aEmpty rhs) | (nm, args, rhs) <- prog]

abstractJe :: ASSOC Name [Name] -- Maps each new SC to the free vars of its
                            -- group
        -> AnnExpr Name (Set Name) -- Input Expression
        -> CoreExpr                 -- Result Expression
abstractJe env (free, ANum n)  = ENum n
abstractJe env (free, AConstr t a)  = EConstr t a
abstractJe env (free, AAp e1 e2)  = EAp (abstractJe env e1) (abstractJe env e2)
abstractJe env (free, AVar g) = foldl EAp (EVar g) (map EVar (aLookup env g []))
abstractJe env (free, ALam as bd) = foldl EAp sc (map EVar fv_list)
    where
        fv_list = actualFreeList env free
        sc = ELet nonrecursive [(toText "sc", sc_rhs)] (EVar $ toText "sc")
        sc_rhs = ELam (fv_list ++ as) (abstractJe env bd)
abstractJe env (free, ACase c alts) = ECase c' alts'
   where
        c' = abstractJe env c
        alts' = [(t,bds,abstractJe env bd) | (t,bds,bd) <- alts]
abstractJe env (free, ALet isrec defns body) =
    ELet isrec (fun_defs' ++ var_defns') body'
    where
        fun_defs = [(name,rhs) | (name, rhs) <- defns, isALam rhs]
        var_defs = [(name,rhs) | (name, rhs) <- defns, not $ isALam rhs]

        fun_names = bindersOf fun_defs
        free_in_funs = setSubtraction
                (setUnionList [freeVarsOf rhs | (_,rhs) <- fun_defs])
                (setFromList fun_names)
        vars_to_abstract = actualFreeList env free_in_funs
        body_env = aConcat (afromList ([(f,vars_to_abstract)| f <- fun_names])) env
        rhs_env | isrec = body_env
                | otherwise = env

        fun_defs' = [(name, ELam (vars_to_abstract ++ args)
                                 (abstractJe rhs_env body))
                    | (name, (free, ALam args body)) <- fun_defs]
        var_defns' =    [(name, abstractJe rhs_env rhs)
                        | (name,rhs) <- var_defs]
        body' = abstractJe body_env body

actualFreeList :: ASSOC Name [Name] -> Set Name -> [Name]
actualFreeList env free = setToList $
            setUnionList
            [ setFromList (aLookup env name [name])
            | name <- setToList free]

isALam :: AnnExpr a b -> Bool
isALam (_, ALam {}) = True
isALam _ = False


