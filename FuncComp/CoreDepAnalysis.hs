module CoreDepAnalysis where

import           CoreLambda
import           CoreLanguage
import           CorePP
import           CoreUtils

depthFirstSearch :: Ord a =>
        (a -> [a]) -- Map
    ->  (Set a, [a])    -- State: visited set,
                        -- current sequence of vertices
    ->  [a]             -- Input
    ->  (Set a, [a])    -- Final State
depthFirstSearch relation inst  = foldl search inst
    where
        search (visited, sequence) vertex
            | setElemOf vertex visited = (visited, sequence)
            | otherwise = (visited', vertex : sequence')
                where
                    (visited', sequence') = depthFirstSearch relation
                        (setUnion visited (setSingleton vertex), sequence)
                        (relation vertex)

spanningSearch :: Ord a =>
                (a -> [a]) -- Map
            ->  (Set a, [Set a]) -- Curr st
            ->  [a]
            ->  (Set a, [Set a])
spanningSearch map st = foldl search st
    where
        search (visited, setSeq) vertex
            | setElemOf vertex visited = (visited, setSeq)
            | otherwise = (visited', setFromList (vertex : sequence) : setSeq)
                where
                    (visited', sequence) = depthFirstSearch map
                        (setUnion visited (setSingleton vertex),[])
                        (map vertex)

-- Strongly connected components
--
scc :: Ord a =>
        (a -> [a]) -- Ins
    ->  (a -> [a]) -- Outs
    ->  [a] -- root vertices
    -> [Set a] -- topologically sorted components
scc ins outs = spanning . depthFirst
    where
        depthFirst = snd . depthFirstSearch outs (setEmpty, [])
        spanning = snd . spanningSearch ins (setEmpty, [])

dependency :: CoreProgram -> CoreProgram
dependency = depends . freeVars

runD = ppprog . dependency

depends :: AnnProgram Name (Set Name) -> CoreProgram
depends prog = [(nm, as, dependse rhs) | (nm,as,rhs) <- prog]

dependse :: AnnExpr Name (Set Name) -> CoreExpr
dependse (free, ANum n) = ENum n
dependse (free, AVar v) = EVar v
dependse (free, AConstr t a) = EConstr t a
dependse (free, AAp e1 e2) = EAp (dependse e1) (dependse e2)
dependse (free, ACase e as) = ECase (dependse e) [(t,as,dependse e) | (t,as,e) <- as]
dependse (free, ALam ns bd) = ELam ns (dependse bd)
dependse (free, ALet isrec defns body) = foldr (mkDependLet isrec) (dependse body) defnGroups
    where
        binders = bindersOf defns
        binderSet   | isrec = setFromList binders
                    | otherwise = setEmpty
        edges = [(n,f)|(n, (free,e)) <- defns, f <- setToList (setIntersection free binderSet)]
        ins v = [u | (u,w) <- edges, v == w]
        outs v = [w | (u,w) <- edges, v == u]
        components = map setToList (scc ins outs binders)
        adefns = afromList defns
        defnGroups = [[ (n, aLookup adefns n (error "defnGroups") )| n <- ns]
                     | ns <- components]

mkDependLet :: Bool -> [(Name, AnnExpr Name (Set Name))] -> CoreExpr -> CoreExpr
mkDependLet isrec dfs e = ELet isrec [(n, dependse e)| (n,e) <- dfs] e
