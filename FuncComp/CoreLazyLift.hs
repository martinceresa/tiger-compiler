module CoreLazyLift where

--- Fully Lazy Lambda Lifter
--
import           CoreLambda
import           CoreLanguage
import           CorePP
import           CoreUtils
----
----
--
type Level = Int

separateLams :: CoreProgram -> CoreProgram
separateLams xs = [(nm, [] ,mkSepLams as $ separateLamse bd) | (nm,as,bd) <- xs]

separateLamse :: CoreExpr -> CoreExpr
separateLamse (EVar v) = EVar v
separateLamse (ENum n) = ENum n
separateLamse (EConstr t a) = EConstr t a
separateLamse (EAp e1 e2) = EAp (separateLamse e1) (separateLamse e2)
separateLamse (ELet rec bds e) = ELet rec [(nm,separateLamse rhs)| (nm,rhs) <- bds]
                                            $ separateLamse e
separateLamse (ECase e alts) = ECase (separateLamse e) $
                                [(i, as, separateLamse e) | (i,as,e) <- alts]
separateLamse (ELam xs e) = mkSepLams xs $ separateLamse e

mkSepLams :: [Name] -> CoreExpr -> CoreExpr
mkSepLams xs bd = foldr (\x bd' -> ELam [x] bd') bd xs

freeSetToLevel :: ASSOC Name Level -> Set Name -> Level
freeSetToLevel env free = foldl max 0 (map (\n -> aLookup env n 0) (setToList free))

addLevels :: CoreProgram -> AnnProgram (Name, Level) Level
addLevels = freeToLevel . freeVars

freeToLevel = map freeToLevelsc

freeToLevelsc (sc_nm,[], rhs) = (sc_nm,[], freeToLevele 0 aEmpty rhs)

freeToLevele :: Level -> -- Level of context
                ASSOC Name Level -> -- Level of in-scope names
                AnnExpr Name (Set Name) -> -- Input expression
                AnnExpr (Name, Level) Level -- Result expression
freeToLevele lvl env (free, ANum k) = (0, ANum k)
freeToLevele lvl env (free, AVar v) = (aLookup env v 0, AVar v)
freeToLevele lvl env (free, AConstr t a) = (0, AConstr t a )
freeToLevele lvl env (free, AAp e1 e2) = (max (levelOf l1) (levelOf l2), AAp l1 l2)
    where
        l1 = freeToLevele lvl env e1
        l2 = freeToLevele lvl env e2
freeToLevele lvl env (free, ALam args bd) = (freeSetToLevel env free, ALam args' body')
    where
        body' = freeToLevele (lvl + 1) (aConcat (afromList args') env) bd
        args' = zip args (repeat (lvl + 1))
freeToLevele lvl env (free, ALet isrec defns bd) = (levelOf new_body, ALet isrec new_defns new_body)
    where
        binders = bindersOf defns
        rhss = rhssOf defns

        new_binders = zip binders (repeat max_rhs_level)
        new_rhss = map (freeToLevele lvl env) rhss
        new_defns = zip new_binders new_rhss
        new_body = freeToLevele lvl env bd

        free_in_rhss = setUnionList (map fst rhss)
        max_rhs_level = freeSetToLevel level_rhs_env free_in_rhss

        body_env = aConcat (afromList new_binders) env
        rhs_env | isrec = body_env
                | otherwise = env
        level_rhs_env   | isrec = aConcat (afromList $ zip binders (repeat (0 :: Int))) env
                        | otherwise = env
freeToLevele lvl env (free, ACase e alts) = freeToLevelcase lvl env free e alts

freeToLevelcase ::  Level ->
                    ASSOC Name Level ->
                    Set Name ->
                    AnnExpr Name (Set Name) ->
                    [AnnAlt Name (Set Name)] ->
                    AnnExpr (Name, Level) Level
freeToLevelcase lvl env free e alts = (max malts (levelOf e'), ACase e' alts')
    where
        e' = freeToLevele lvl env e
        (malts,alts') = foldr (\(i,bds,bd) (mi,as) ->
                                let bds' = zip bds (repeat (0 :: Int))
                                    bd' = freeToLevele lvl (aConcat (afromList bds') env) bd
                                in (max mi (levelOf bd'),(i,bds',bd'):as)) (0,[]) alts

levelOf :: AnnExpr a Level -> Level
levelOf = fst

identifyMFEs :: AnnProgram (Name, Level) Level -> Program (Name,Level)
identifyMFEs progs = [ (sc_nm,[], identifyMFEse 0 rhs) |(sc_nm, [], rhs) <- progs]
--

notMFECandidate :: AnnExpr' a b -> Bool
notMFECandidate (AVar t) = True
notMFECandidate (ANum t) = True
notMFECandidate (AConstr t1 t2) = True
notMFECandidate _ = False

identifyMFEse :: Level -- Level of context
                -> AnnExpr (Name,Level) Level -- Input expression
                -> Expr (Name, Level)
identifyMFEse ctx (lvl, e)  | lvl == ctx || notMFECandidate e = e'
                            | otherwise = transformMFE lvl e'
    where
        e' = identifyMFEse1 lvl e

transformMFE :: Level -> Expr (Name, Level) -> Expr (Name, Level)
transformMFE lvl e = ELet False [((toText "v",lvl),e)] (EVar (toText "v"))

identifyMFEse1 :: Level -> AnnExpr' (Name, Level) Level -> Expr (Name, Level)
identifyMFEse1 lvl (AVar v) = EVar v
identifyMFEse1 lvl (ANum n) = ENum n
identifyMFEse1 lvl (AConstr t a) = EConstr t a
identifyMFEse1 lvl (AAp e1 e2) = EAp (identifyMFEse lvl e1) (identifyMFEse lvl e2)
identifyMFEse1 lvl (ALam as bd) = ELam as (identifyMFEse al bd)
    where
        ((_, al):_) = as
identifyMFEse1 lvl (ALet isrec defns body) = ELet isrec defns' body'
    where
        body' = identifyMFEse lvl body
        defns' = [ ((nm, rhs_lvl), identifyMFEse rhs_lvl rhs) | ((nm, rhs_lvl),rhs) <- defns]
identifyMFEse1 lvl (ACase c alts) = ECase c' alts'
    where
        c' = identifyMFEse lvl c
        alts' = [(i,bds,identifyMFEse (snd $ head bds) a)| (i, bds, a) <- alts]

renameL :: Program (Name, a) -> Program (Name, a)
renameL = renameGen newNamesL

newNamesL :: NameSupply -> [(Name,a)] -> (NameSupply, [(Name,a)], ASSOC Name Name)
newNamesL ns old_bds = (ns', new_bds, env)
    where
        old_names = map fst old_bds
        lvls = map snd old_bds
        (ns', new_names) = getNames ns old_names
        new_bds = zip new_names lvls
        env = afromList $ zip old_names new_names
-- FLOATTTT
--
float :: Program (Name, Level) -> CoreProgram
float = concat . (map float_sc)

float_sc (nm,[],rhs) = [(nm,[],rhs')] ++ concat (map to_sc fds)
    where
        (fds, rhs') = floate rhs
        to_sc (_,_, defns) = map make_sc defns
        make_sc (nm,rhs) = (nm,[],rhs)

type FloatedDefns = [(Level, IsRec, [(Name, Expr Name)])]

floate :: Expr (Name, Level) -> (FloatedDefns, Expr Name)
floate (EVar v) = ([], EVar v)
floate (ENum n) = ([], ENum n)
floate (EConstr t a) = ([], EConstr t a)
floate (EAp e1 e2) = (fl1 ++ fl2, EAp e1' e2')
    where
        (fl1, e1') = floate e1
        (fl2, e2') = floate e2
floate (ELam as bd) = (fd_outer, ELam app (install fd_this ee))
    where
        ELam app ee = mkElam as' bd'
        as' = map fst as
        (fst_a, lvl):_  = as
        (flt_bd, bd') = floate bd
        (fd_outer, fd_this) = partitionFloats lvl flt_bd
floate (ELet isrec bds bd) = (rhsFloateds ++ [thisGroup] ++ bd_floated, bd')
    where
        (bd_floated, bd') = floate bd
        (rhsFloateds, bds') = mapAccuml float_defn [] bds
        thisGroup = (thisLevel, isrec, bds')
        (name, thisLevel) = head $ bindersOf bds

        float_defn floatedDefns ((name,_), rhs) =
            (frhs ++ floatedDefns, (name,rhs'))
            where
                (frhs, rhs') = floate rhs
floate (ECase e alt) = (frhs ++ floatedE,ECase e' alt')
    where
        (floatedE, e') = floate e
        (frhs, alt') = foldr (\(i, bds, e) (fls,as)
                        -> let (f,e') = floate e in
                            (f++fls, (i, map fst bds, e') : as)) ([],[]) alt

mkElam :: [Name] -> CoreExpr -> CoreExpr
mkElam as (ELam as' bd) = ELam (as ++ as') bd
mkElam as e = ELam as e

partitionFloats :: Level -> FloatedDefns -> (FloatedDefns, FloatedDefns)
partitionFloats this_lvl fds = (filter nin_this fds, filter in_this fds)
    where
        in_this (lvl, isrec, defns) = lvl >= this_lvl
        nin_this (lvl,isrec,defns)  = lvl < this_lvl

install :: FloatedDefns -> Expr Name -> Expr Name
install defnGroups e = foldr installGroup e defnGroups
    where
        installGroup (lvl,isrec,defns) e = ELet isrec defns e
--
-- fullyLazyLift = float . renameL . identifyMFEs . addLevels . separateLams
--
-- runF = ppprog . lambdaLift . fullyLazyLift
